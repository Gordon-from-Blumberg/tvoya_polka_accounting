#!/bin/sh

echo "shutdown tomcat"
sh /opt/tomcat/bin/shutdown.sh

backupName="backup-$(date +%Y.%m.%d-%H.%M).zip"
echo "make backup ${backupName}"
zip -rm "/var/backups/tomcat/${backupName}" /opt/tomcat/webapps/ROOT.war /opt/tomcat/conf/ROOT /opt/tomcat/conf/Catalina/localhost/ROOT.xml

echo "unzip deploy.zip"
unzip -o /opt/tomcat/_deploy/deploy.zip -d /opt/tomcat

echo "wait for 5 sec"
sleep 5

if [ -f /opt/tomcat/bin/catalina.pid ]
then
    echo "tomcat did not stop, kill the process"
    kill -9 $(cat /opt/tomcat/bin/catalina.pid)
fi

echo "start tomcat"
sh /opt/tomcat/bin/startup.sh

#systemctl restart tomcat
