package com.tvoyapolka.accounting.web.utils;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 12.04.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.gui.components.Field;
import com.tvoyapolka.accounting.entity.Product;
import com.tvoyapolka.accounting.entity.Prolongation;
import com.tvoyapolka.accounting.utils.BusinessUtils;

public class WebBusinessUtils {
    private WebBusinessUtils() {}

    public static void adjustShelfField(Field shelfField, Prolongation prolongation) {
        final boolean isShelfRequired = BusinessUtils.isShelfProlongation(prolongation);
        shelfField.setRequired(isShelfRequired);
        shelfField.setEnabled(isShelfRequired);
    }

    public static void adjustShelfField(Field shelfField, Product product) {
        final boolean isShelfRequired = product != null && BusinessUtils.isShelfProduct(product);
        shelfField.setRequired(isShelfRequired);
        shelfField.setEnabled(isShelfRequired);
    }
}
