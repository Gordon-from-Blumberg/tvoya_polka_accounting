package com.tvoyapolka.accounting.web.utils;

import com.haulmont.cuba.gui.components.FieldGroup;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;

import java.util.List;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 02.05.19
 * <p>
 * $Id$
 */

public class WebComponentUtils {
    private static Log log = LogFactory.getLog(WebComponentUtils.class);

    private WebComponentUtils() {}

    public static void setFieldGroupTabIndexes(FieldGroup fieldGroup, int startIndex, int colCoef) {
        for (int col = 0; col < fieldGroup.getColumns(); col++) {
            final List<FieldGroup.FieldConfig> columnFields = fieldGroup.getFields(col);
            for (int row = 0; row < columnFields.size(); row++) {
                columnFields.get(row).setTabIndex(col * colCoef + row + startIndex);
                log.debug("setFieldGroupTabIndexes: set to field %s tabIndex %s",
                        columnFields.get(row).getProperty(), col * colCoef + row + startIndex);
            }
        }
    }

    public static void setFieldGroupTabIndexes(FieldGroup fieldGroup, int startIndex) {
        setFieldGroupTabIndexes(fieldGroup, startIndex, calcColCoef(fieldGroup));
    }

    public static void setFieldGroupTabIndexes(FieldGroup fieldGroup) {
        setFieldGroupTabIndexes(fieldGroup, 1, calcColCoef(fieldGroup));
    }

    private static int calcColCoef(FieldGroup fieldGroup) {
        int colCoef = 0;
        for (int col = 0; col < fieldGroup.getColumns(); col++) {
            final int rowsCount = fieldGroup.getFields(col).size();
            if (rowsCount > colCoef) {
                colCoef = rowsCount;
            }
        }
        return colCoef + 1;
    }
}