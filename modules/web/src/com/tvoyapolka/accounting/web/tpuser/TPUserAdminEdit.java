package com.tvoyapolka.accounting.web.tpuser;

import com.haulmont.cuba.core.global.*;
import com.haulmont.cuba.gui.WindowParam;
import com.haulmont.cuba.gui.components.AbstractEditor;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.data.Datasource;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.security.entity.UserRole;
import com.tvoyapolka.accounting.constants.Roles;
import com.tvoyapolka.accounting.entity.Director;
import com.tvoyapolka.accounting.entity.Shop;
import com.tvoyapolka.accounting.entity.TPUser;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;
import com.tvoyapolka.accounting.properties.PropertiesHelper;
import com.tvoyapolka.accounting.service.ShopService;
import com.tvoyapolka.accounting.service.TPEmailService;
import com.tvoyapolka.accounting.utils.MapUtils;
import com.tvoyapolka.accounting.utils.PasswordGenerator;
import com.tvoyapolka.accounting.web.component.UserStatusComponent;
import org.apache.commons.lang.BooleanUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.UUID;

import static com.tvoyapolka.accounting.web.component.UserStatusComponent.*;

public class TPUserAdminEdit extends AbstractEditor<TPUser> {

    @Inject
    protected Metadata metadata;
    @Inject
    protected TPEmailService tpEmailService;
    @Inject
    protected PasswordEncryption passwordEncryption;
    @Inject
    protected UserSessionSource userSessionSource;
    @Inject
    protected UserStatusComponent userStatusComponent;
    @Inject
    protected ShopService shopService;

    @Named("directorDs")
    protected CollectionDatasource<Director, UUID> directorDs;
    @Named("shopsDs")
    protected CollectionDatasource<Shop, UUID> shopsDs;
    @Named("userRolesDs")
    protected CollectionDatasource<UserRole, UUID> userRolesDs;
    @Named("status")
    protected LookupField statusLookup;
    @Named("shop")
    protected LookupField shopLookup;

    @WindowParam
    protected Boolean newDirector;

    private Log log = LogFactory.getLog(getClass());

    private String password;

    @Override
    public void init(Map<String, Object> params) {
        statusLookup.setOptionsMap(MapUtils.<String, String>start()
                .add(getMessage(ADMIN), ADMIN)
                .add(getMessage(DIRECTOR), DIRECTOR)
                .add(getMessage(SELLER), SELLER)
                .build());
        if (BooleanUtils.isTrue(newDirector)) {
            statusLookup.setValue(DIRECTOR);
            statusLookup.setEnabled(false);
            shopLookup.setEnabled(false);
        } else {
            statusLookup.addValueChangeListener(this::onStatusChange);
            shopLookup.addValueChangeListener(e -> onShopChange((Shop) e.getValue()));
        }
    }

    @Override
    protected void initNewItem(TPUser item) {
        final User newUser = metadata.create(User.class);
        newUser.setGroup( userSessionSource.getUserSession().getUser().getGroup() );
        newUser.setTimeZoneAuto(true);
        newUser.setChangePasswordAtNextLogon(!PropertiesHelper.isDevEnvironment());
        password = PropertiesHelper.isDevEnvironment() ? "1" : PasswordGenerator.generate();
        newUser.setPassword(passwordEncryption.getPasswordHash(newUser.getId(), password));
        item.setUser(newUser);
    }

    @Override
    protected void postInit() {
        final User user = getItem().getUser();
        if (!PersistenceHelper.isNew(user)) {
            final String statusCode = userStatusComponent.getUserStatusCode(user);
            if (!UNKNOWN.equals(statusCode)) {
                statusLookup.setValue(statusCode);
                statusLookup.setEnabled(isAdmin());
            }

            shopLookup.setValue(shopService.findShopByGroup(user.getGroup()));
        }
    }

    @Override
    protected boolean postCommit(boolean committed, boolean close) {
        if (committed && password != null) {
            if (!PropertiesHelper.isDevEnvironment()) {
                final User user = getItem().getUser();
                try {
                    tpEmailService.sendPasswordEmail(user.getEmail(), user.getLogin(), password);
                    showNotification(String.format(getMessage("email.successfullySent"), user.getLogin(), user.getEmail()));
                } catch (EmailException e) {
                    throw new RuntimeException(getMessage("email.error.notSent"), e);
                }
            } else {
                showNotification("Password set to 1");
            }
        }

        return true;
    }

    private void onStatusChange(ValueChangeEvent event) {
        if (event.getValue() == null) {
            return;
        }

        final String newStatusCode = (String) event.getValue();

        if (ADMIN.equals(newStatusCode) && !isAdmin()) {
            showNotification(getMessage("status.error.onlyAdminCanAppointAdmin"), NotificationType.ERROR);
            event.getComponent().setValue(event.getPrevValue());
        }

        if (directorDs.getState() == Datasource.State.INVALID) {
            directorDs.refresh();
        }

        if (DIRECTOR.equals(newStatusCode) && directorDs.size() != 1) {
            showNotification(getMessage("status.error.isNotDirector"), NotificationType.ERROR);
            event.getComponent().setValue(event.getPrevValue());
            return;
        }

        setRoles(newStatusCode);

        if (ADMIN.equals(newStatusCode)) {
            getItem().getUser().setGroup( userSessionSource.getUserSession().getUser().getGroup() );
        }

        shopLookup.setEnabled(SELLER.equals(newStatusCode));

        shopsDs.setQuery(getShopsDsQuery(newStatusCode));
        shopsDs.refresh();

        if (shopLookup.getValue() != null) {
            setGroup(getItem().getUser(), newStatusCode, shopLookup.getValue());
        }
    }

    private void onShopChange(Shop shop) {
        if (statusLookup.getValue() != null) {
            setGroup(getItem().getUser(), statusLookup.getValue(), shop);
        }
    }

    private void setRoles(String statusCode) {
        userStatusComponent.setRoles(getItem().getUser(), statusCode);
        userRolesDs.suspendListeners();
        try {
            getItem()
                    .getUser()
                    .getUserRoles()
                    .forEach(userRolesDs::addItem);
        } finally {
            userRolesDs.resumeListeners();
        }
    }

    private void setGroup(User user, String statusCode, Shop shop) {
        switch (statusCode) {
            case ADMIN:
                user.setGroup( userSessionSource.getUserSession().getUser().getGroup() );
                break;
            case DIRECTOR:
                user.setGroup(shop.getDirector().getGroup());
                break;
            case SELLER:
                user.setGroup( shop.getGroup() );
                break;
            default:
                throw new IllegalStateException(formatMessage("status.error.groupIsNotSet",
                        user.getLogin(), getMessage(statusCode)));
        }
    }

    private String getShopsDsQuery(String status) {
        final String queryTemlate = "select sh from tpa$Shop sh %s";

        switch (status) {
            case ADMIN:
                return String.format(queryTemlate, "");
            case DIRECTOR:
                return String.format(queryTemlate, "where sh.director.tpUser.id = :ds$tPUserDs.id");
            case SELLER:
                return String.format(queryTemlate, isAdmin()
                        ? ""
                        : "where sh.director.id = :session$directorId");
            default:
                throw new IllegalStateException(formatMessage("status.error.shopsNotLoaded",
                        getItem().getUser().getLogin(), getMessage(status)));
        }
    }

    private boolean isAdmin() {
        return userSessionSource.getUserSession().getRoles().contains(Roles.ADMINISTRATORS_ROLE);
    }
}