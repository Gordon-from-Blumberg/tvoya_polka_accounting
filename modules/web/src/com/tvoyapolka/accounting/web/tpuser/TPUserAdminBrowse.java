package com.tvoyapolka.accounting.web.tpuser;

import com.haulmont.bali.util.ParamsMap;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.EmailException;
import com.haulmont.cuba.core.global.PasswordEncryption;
import com.haulmont.cuba.gui.WindowParam;
import com.haulmont.cuba.gui.components.AbstractLookup;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.components.actions.CreateAction;
import com.haulmont.cuba.gui.components.actions.EditAction;
import com.haulmont.cuba.gui.data.GroupDatasource;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.haulmont.cuba.security.entity.User;
import com.tvoyapolka.accounting.entity.TPUser;
import com.tvoyapolka.accounting.properties.PropertiesHelper;
import com.tvoyapolka.accounting.service.TPEmailService;
import com.tvoyapolka.accounting.utils.PasswordGenerator;
import com.tvoyapolka.accounting.web.component.UserStatusComponent;
import org.apache.commons.lang.BooleanUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.UUID;

public class TPUserAdminBrowse extends AbstractLookup {
    private static final String EDITOR_SCREEN_ID = "tpa$TPUser.admin.edit";

    @Inject
    protected DataManager dataManager;
    @Inject
    protected ComponentsFactory componentsFactory;
    @Inject
    protected PasswordEncryption passwordEncryption;
    @Inject
    protected UserStatusComponent userStatusComponent;
    @Inject
    protected TPEmailService tpEmailService;

    @Named("tPUsersDs")
    protected GroupDatasource<TPUser, UUID> tPUsersDs;

    @Named("tPUsersTableAdmin")
    protected GroupTable<TPUser> tPUsersTable;
    @Named("tPUsersTableAdmin.create")
    protected CreateAction createAction;
    @Named("tPUsersTableAdmin.edit")
    protected EditAction editAction;

    @WindowParam
    protected Boolean newDirector;

    @Override
    public void init(Map<String, Object> params) {
        createAction.setWindowId(EDITOR_SCREEN_ID);
        editAction.setWindowId(EDITOR_SCREEN_ID);

        if (BooleanUtils.isTrue(newDirector)) {
            createAction.setWindowParams(ParamsMap.of("newDirector", newDirector));
        }
    }

    @SuppressWarnings("unused")
    public Table.PlainTextCell generateStatusColumn(TPUser tpUser) {
        return new Table.PlainTextCell(
                getMessage(userStatusComponent.getUserStatusCode(tpUser.getUser()))
        );
    }

    public void resetPassword() {
        final TPUser selectedTPUser = tPUsersTable.getSingleSelected();

        if (selectedTPUser != null) {
            final User user = selectedTPUser.getUser();
            final String newPassword = PropertiesHelper.isDevEnvironment() ? "1" : PasswordGenerator.generate();
            user.setPassword(passwordEncryption.getPasswordHash(user.getId(), newPassword));
            user.setChangePasswordAtNextLogon(!PropertiesHelper.isDevEnvironment());
            selectedTPUser.setUser(dataManager.commit(user, "user.tpUserAdminBrowser"));
            tPUsersDs.updateItem(selectedTPUser);
            if (!PropertiesHelper.isDevEnvironment()) {
                try {
                    tpEmailService.sendPasswordEmail(user.getEmail(), user.getLogin(), newPassword);
                    showNotification(String.format(getMessage("email.successfullySent"), user.getLogin(), user.getEmail()));
                } catch (EmailException e) {
                    throw new RuntimeException(getMessage("email.error.notSent"), e);
                }
            } else {
                showNotification("Password set to 1");
            }
        }
    }
}