package com.tvoyapolka.accounting.web.component;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 21.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.EmailException;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.PasswordEncryption;
import com.haulmont.cuba.gui.components.AbstractFrame;
import com.haulmont.cuba.gui.components.Frame;
import com.haulmont.cuba.security.entity.User;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;
import com.tvoyapolka.accounting.properties.PropertiesHelper;
import com.tvoyapolka.accounting.service.TPEmailService;
import com.tvoyapolka.accounting.utils.PasswordGenerator;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.function.Consumer;


@Component
public class ResetPasswordComponent {
    private static final String DEV_PASSWORD = "1";

    @Inject
    protected PasswordEncryption passwordEncryption;
    @Inject
    protected Messages messages;
    @Inject
    protected TPEmailService tpEmailService;

    private Log log = LogFactory.getLog(getClass());

    public void resetPassword(User user, Consumer<User> consumer, AbstractFrame screen) {
        final boolean isDev = PropertiesHelper.isDevEnvironment();
        final String newPassword = isDev ? DEV_PASSWORD : PasswordGenerator.generate();
        user.setPassword(passwordEncryption.getPasswordHash(user.getId(), newPassword));
        user.setChangePasswordAtNextLogon(!isDev);

        consumer.accept(user);

        if (!isDev) {
            try {
                tpEmailService.sendPasswordEmail(user.getEmail(), user.getLogin(), newPassword);
                screen.showNotification(String.format(getMessage("email.successfullySent"), user.getLogin(), user.getEmail()));
            } catch (EmailException e) {
                log.error(new RuntimeException(e), "resetPassword: email was not sent");
                screen.showNotification(getMessage("email.error.notSent"), Frame.NotificationType.ERROR);
            }
        } else {
            screen.showNotification("Password set to " + DEV_PASSWORD);
        }
    }

    private String getMessage(String key) {
        return messages.getMainMessage(key);
    }
}
