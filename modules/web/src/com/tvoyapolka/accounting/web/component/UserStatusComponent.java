package com.tvoyapolka.accounting.web.component;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 19.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.global.View;
import com.haulmont.cuba.security.entity.Role;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.security.entity.UserRole;
import com.tvoyapolka.accounting.constants.Roles;
import com.tvoyapolka.accounting.utils.CollectionUtils;
import com.tvoyapolka.accounting.utils.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class UserStatusComponent {
    public static final String ADMIN = "status.administrator";
    public static final String DIRECTOR = "status.director";
    public static final String SELLER = "status.seller";
    public static final String UNKNOWN = "status.unknown";

    private static final Map<String, List<String>> ROLES_MAP = MapUtils.<String, List<String>>start()
            .add(ADMIN, Arrays.asList(Roles.ADMINISTRATORS_ROLE))
            .add(DIRECTOR, Arrays.asList(Roles.DIRECTOR_ROLE, Roles.SELLER_ROLE))
            .add(SELLER, Arrays.asList(Roles.SELLER_ROLE))
            .build();

    @Inject
    protected DataManager dataManager;
    @Inject
    protected Metadata metadata;

    public String getUserStatusCode(User user) {
        if (user.getUserRoles() == null) {
            return SELLER;
        }

        final List<String> userRoles = user
                .getUserRoles()
                .stream()
                .map(ur -> ur.getRole().getName())
                .collect(Collectors.toList());

        if (userRoles.contains(Roles.ADMINISTRATORS_ROLE)) {
            return ADMIN;
        } else if (userRoles.contains(Roles.DIRECTOR_ROLE)) {
            return DIRECTOR;
        } else if (userRoles.contains(Roles.SELLER_ROLE)) {
            return SELLER;
        } else {
            return UNKNOWN;
        }
    }

    public void setRoles(User user, String status) {
        final List<String> roleNames = ROLES_MAP.get(status);

        final Collection<Role> allRoles = dataManager.loadList(LoadContext.create(Role.class)
                .setView(View.LOCAL)
                .setQuery(new LoadContext.Query("select r from sec$Role r")));

        final List<UserRole> userRoles = user.getUserRoles();

        final List<UserRole> newUserRoles = userRoles != null
                ? userRoles
                    .stream()
                    .filter(ur -> BooleanUtils.isTrue(ur.getRole().getDefaultRole())
                            || roleNames.contains(ur.getRole().getName()))
                    .collect(Collectors.toList())
                : new ArrayList<>();

        for (String roleName : roleNames) {
            if (newUserRoles.stream().noneMatch(nur -> roleName.equals(nur.getRole().getName()))) {
                final UserRole userRole = metadata.create(UserRole.class);
                userRole.setUser(user);
                userRole.setRole(CollectionUtils.find(allRoles, r -> roleName.equals(r.getName())));
                newUserRoles.add(userRole);
            }
        }

        user.setUserRoles(newUserRoles);
    }
}
