package com.tvoyapolka.accounting.web.shop;

import com.haulmont.bali.util.ParamsMap;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.actions.CreateAction;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.data.Datasource;
import com.tvoyapolka.accounting.entity.Director;
import com.tvoyapolka.accounting.entity.Shelf;
import com.tvoyapolka.accounting.entity.Shop;
import com.tvoyapolka.accounting.entity.TPUser;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;
import com.tvoyapolka.accounting.service.ShopInitService;
import com.tvoyapolka.accounting.web.shelf.ShelfMultipleCreate;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class ShopAdminEdit extends AbstractEditor<Shop> {
    private static final String LOOKUP_SCREEN_ID = "tpa$TPUser.admin.browse";

    @Inject
    protected Metadata metadata;
    @Inject
    protected DataManager dataManager;
    @Inject
    protected ShopInitService shopInitService;

    @Named("directorDs")
    protected Datasource<Director> directorDs;
    @Named("shelvesDs")
    protected CollectionDatasource<Shelf, UUID> shelvesDs;

    @Named("directorLookupField.lookup")
    protected PickerField.LookupAction lookupAction;
    @Named("directorLookupField")
    protected LookupPickerField lookupPickerField;
    @Named("directorEditButton")
    protected Button directorEditButton;
    @Named("rentPlaceTypesTableButtons")
    protected ButtonsPanel rentPlaceTypesTableButtons;
    @Named("shelvesTable.create")
    protected CreateAction shelvesTableCreateAction;

    private Log log = LogFactory.getLog(getClass());

    @Override
    public void init(Map<String, Object> params) {
        lookupAction.setLookupScreen(LOOKUP_SCREEN_ID);
        lookupAction.setLookupScreenParams(ParamsMap.of("newDirector", Boolean.TRUE));

        lookupPickerField.addValueChangeListener(new DirectorChangeListener());

        shelvesTableCreateAction.setInitialValuesSupplier(() -> ParamsMap.of("number", getNextShelfNumber()));
    }

    @Override
    protected void initNewItem(Shop item) {
        item.setRentPlaceTypes(shopInitService.createStandardRentPlaceTypeList(item));
        item.setShelves(new ArrayList<>());
        item.setArticulBase(shopInitService.getNextArticulBase());
    }

    @Override
    protected void postInit() {
        final Director director = getItem().getDirector();
        if (director != null) {
            lookupPickerField.setValue(director.getTpUser());
            directorEditButton.setEnabled(true);
        }

        rentPlaceTypesTableButtons.setEnabled(director != null);
    }

    @SuppressWarnings("unused")
    public void onEditDirector(Component source) {
        openEditor(
                "tpa$Director.admin.edit",
                getItem().getDirector(),
                WindowManager.OpenType.DIALOG
        );
    }

    @SuppressWarnings("unused")
    public void multipleCreate(Component source) {
        ShelfMultipleCreate window = (ShelfMultipleCreate) openWindow("tpa$Shelf.multipleCreate", WindowManager.OpenType.DIALOG);
        window.addCloseListener(action -> {
            if (ShelfMultipleCreate.CREATE_ACTION.equals(action)) {
                createMultipleShelves(window.getFrom(), window.getTo());
            }
        });
    }

    protected int getNextShelfNumber() {
        int[] numbers = getItem().getShelves()
                .stream()
                .mapToInt(Shelf::getNumber)
                .sorted()
                .toArray();

        int nextNumber = 1;
        for (int n : numbers) {
            if (nextNumber != n) {
                return nextNumber;
            }
            nextNumber++;
        }
        return nextNumber;
    }

    protected void createMultipleShelves(Integer from, Integer to) {
        if (from == null || to == null) {
            return;
        }

        final List<Integer> existingNumbers = getItem().getShelves()
                .stream()
                .map(Shelf::getNumber)
                .collect(Collectors.toList());
        for (int number = from; number <= to; number++) {
            if (!existingNumbers.contains(number)) {
                shelvesDs.addItem(createShelf(number));
            }
        }
    }

    protected Shelf createShelf(Integer number) {
        final Shelf shelf = metadata.create(Shelf.class);
        shelf.setShop(getItem());
        shelf.setNumber(number);
        return shelf;
    }

    private class DirectorChangeListener implements ValueChangeListener {
        @Override
        public void valueChanged(ValueChangeEvent e) {
            directorEditButton.setEnabled(e.getValue() != null);
            rentPlaceTypesTableButtons.setEnabled(e.getValue() != null);

            if (e.getValue() == null || e.getValue().equals(e.getPrevValue())) {
                return;
            }

            final TPUser tpUser = (TPUser) e.getValue();
            Director director = findDirector(tpUser);
            if (director == null) {
                director = metadata.create(Director.class);
                director.setTpUser(tpUser);
            }

            getItem().setDirector(director);
            directorDs.setItem(director);
        }

        private Director findDirector(TPUser tpUser) {
            LoadContext<Director> ctx = LoadContext
                    .create(Director.class)
                    .setQuery(LoadContext.createQuery("select d from tpa$Director d where d.tpUser.id = :tpUserId")
                        .setParameter("tpUserId", tpUser.getId()))
                    .setView("director-name-view");
            Director director = dataManager.load(ctx);
            log.debug("DirectorChangeListener.findDirector: for user %s found %s", tpUser, director);
            return director;
        }
    }
}