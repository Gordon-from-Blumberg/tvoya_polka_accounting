package com.tvoyapolka.accounting.web.shop;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.gui.components.AbstractLookup;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.components.actions.CreateAction;
import com.haulmont.cuba.gui.components.actions.EditAction;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.tvoyapolka.accounting.entity.Shelf;
import com.tvoyapolka.accounting.entity.ShelfState;
import com.tvoyapolka.accounting.entity.Shop;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;
import com.tvoyapolka.accounting.utils.CollectionUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Map;

public class ShopAdminBrowse extends AbstractLookup {
    private static final String EDITOR_SCREEN_ID = "tpa$Shop.admin.edit";

    @Inject
    protected ComponentsFactory componentsFactory;
    @Inject
    protected DataManager dataManager;

    @Named("shopsAdminTable.create")
    protected CreateAction createAction;
    @Named("shopsAdminTable.edit")
    protected EditAction editAction;

    private Log log = LogFactory.getLog(getClass());

    @Override
    public void init(Map<String, Object> params) {
        createAction.setWindowId(EDITOR_SCREEN_ID);
        editAction.setWindowId(EDITOR_SCREEN_ID);
    }

    @SuppressWarnings("unused")
    public Table.PlainTextCell generateContragentCountField(Shop shop) {
        long start = System.currentTimeMillis();
        final Table.PlainTextCell textCell = new Table.PlainTextCell(
                dataManager.loadValue(
                        "select count(c) from tpa$Contragent c where c.active = true and c.shop.id = :shopId",
                        Integer.class
                ).parameter("shopId", shop.getId())
                        .optional()
                        .orElse(0)
                        .toString()
        );
        log.debug("generateContragentCountField: loading of contragent count took %s ms", System.currentTimeMillis() - start);
        return textCell;
    }

    @SuppressWarnings("unused")
    public Table.PlainTextCell generateShelfCountField(Shop shop) {
        long start = System.currentTimeMillis();
        final List<Shelf> shelves = shop.getShelves();
        final Table.PlainTextCell textCell = new Table.PlainTextCell(
                String.format(
                        "%s (%s - %s)",
                        shelves.size(),
                        CollectionUtils.count(shelves, sh -> sh.getState() == ShelfState.RENTED),
                        CollectionUtils.count(shelves, sh -> sh.getState() == ShelfState.FREE)
                )
        );
        log.debug("generateShelfCountField: counting of shelves took %s ms", System.currentTimeMillis() - start);
        return textCell;
    }
}