package com.tvoyapolka.accounting.web.invoice;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 04.04.19
 * <p>
 * $Id$
 */

import com.haulmont.bali.util.ParamsMap;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.global.View;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.WindowParam;
import com.haulmont.cuba.gui.WindowParams;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.actions.CreateAction;
import com.haulmont.cuba.gui.components.actions.EditAction;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.data.Datasource;
import com.haulmont.cuba.gui.data.DsBuilder;
import com.haulmont.cuba.gui.data.aggregation.AggregationStrategy;
import com.haulmont.cuba.gui.data.impl.DatasourceImplementation;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.tvoyapolka.accounting.entity.*;
import com.tvoyapolka.accounting.utils.BusinessUtils;
import com.tvoyapolka.accounting.web.listener.ProductCostChangeListener;
import com.tvoyapolka.accounting.web.utils.WebBusinessUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.*;

import static com.haulmont.cuba.core.global.PersistenceHelper.isNew;

public class InvoiceCreate extends AbstractEditor<Invoice> {

    @Inject
    protected ComponentsFactory componentsFactory;
    @Inject
    protected Metadata metadata;

    @Named("invoiceProductsDs")
    protected CollectionDatasource<InvoiceProduct, UUID> invoiceProductsDs;
    @Named("productDs")
    protected DatasourceImplementation<Product> productDs;
    @Named("existingProductsDs")
    protected CollectionDatasource<Product, UUID> existingProductsDs;
    @Named("rentPlaceTypesDs")
    protected CollectionDatasource<RentPlaceType, UUID> rentPlaceTypesDs;
    @Named("shelvesDs")
    protected CollectionDatasource<Shelf, UUID> shelvesDs;

    @Named("caption")
    protected Label caption;
    @Named("invoiceTypeBox")
    protected BoxLayout invoiceTypeBox;
    @Named("orderCostBox")
    protected BoxLayout orderCostBox;
    @Named("invoiceTypeField")
    protected LookupField invoiceTypeField;
    @Named("productsTable.create")
    protected CreateAction productsTableCreateAction;
    @Named("productsTable.edit")
    protected EditAction productsTableEditAction;
    @Named("productsTable")
    protected Table<InvoiceProduct> productsTable;

    @WindowParam
    protected InvoiceType invoiceType;

    private final Map<InvoiceProduct, Datasource<InvoiceProduct>> DS_MAP = new HashMap<>();

    @Override
    public void init(Map<String, Object> params) {
        existingProductsDs.refresh();

        Map<String, Object> paramsMap = ParamsMap.of(
                "rentPlaceTypesDs", rentPlaceTypesDs,
                "shelvesDs", shelvesDs,
                "existingProductsDs", existingProductsDs
        );
        productsTableCreateAction.setWindowId("tpa$InvoiceProduct.create");
        productsTableCreateAction.setWindowParams(paramsMap);
        productsTableEditAction.setWindowId("tpa$InvoiceProduct.create");
        productsTableEditAction.setWindowParams(paramsMap);

        Table.Column totalCostColumn = productsTable.getColumn("totalCost");
        AggregationInfo aggregationInfo = new AggregationInfo();
        aggregationInfo.setType(AggregationInfo.Type.CUSTOM);
        aggregationInfo.setStrategy(new TotalCostAggregationStrategy());
        totalCostColumn.setAggregation(aggregationInfo);
        totalCostColumn.setValueDescription(getMessage("common.sum"));

        invoiceTypeField.addValueChangeListener(e -> onInvoiceTypeChanged( (InvoiceType) e.getValue() ));

        final Invoice invoice = WindowParams.ITEM.<Invoice>get(params);
        if (invoiceType != null) {
            invoice.setType(invoiceType);
            invoiceTypeBox.setVisible(false);
        }

        invoiceProductsDs.addCollectionChangeListener(e -> {
            if (e.getOperation() == CollectionDatasource.Operation.ADD) {
                invoiceTypeField.setEditable(false);
            }
        });

        if (invoice.getContract().getContragent().getType() == ContragentType.SPECIAL) {
            orderCostBox.setVisible(true);
            ((Field) getComponentNN("orderCostField")).setRequired(true);
        }
    }

    @Override
    protected void initNewItem(Invoice item) {
        item.setProducts(new ArrayList<>());
        if (invoiceType == null && item.getType() == null) {
            item.setType(InvoiceType.IN);
        }
    }

    @Override
    protected void postInit() {
        onInvoiceTypeChanged(getItem().getType());
    }

    @Override
    protected boolean preCommit() {
        final Invoice invoice = getItem();
        if (invoice.getType() != InvoiceType.CHANGE_COST) {
            invoice.setTotalCost( BusinessUtils.calculateInvoiceTotalCost(invoice.getProducts()) );
        } else  {
            List<InvoiceProduct> invoiceProducts = new ArrayList<>(invoice.getProducts());
            invoiceProducts.forEach(this::createProductWithNewCost);
        }
        return true;
    }

    @SuppressWarnings("unused")
    public TextInputField.MaxLengthLimited generateNameField(InvoiceProduct invoiceProduct) {
        return createTextInputField(invoiceProduct, TextField.class, "name", true, null, false);
    }

    @SuppressWarnings("unused")
    public TextInputField.MaxLengthLimited generateDescriptionField(InvoiceProduct invoiceProduct) {
        return createTextInputField(invoiceProduct, TextField.class, "description", false, 30, true);
    }

    @SuppressWarnings("unused")
    public TextInputField.MaxLengthLimited generateCostField(InvoiceProduct invoiceProduct) {
        TextInputField.MaxLengthLimited field = createTextInputField(invoiceProduct, TextField.class, "cost", true, null, false);
        field.addValueChangeListener(new ProductCostChangeListener(
                existingProductsDs,
                products -> openLookup(
                        "existing-product-list",
                        selectedProducts -> {
                            if (selectedProducts != null && !selectedProducts.isEmpty()) {
                                invoiceProduct.setProduct((Product) selectedProducts.iterator().next());
                            }
                        },
                        WindowManager.OpenType.DIALOG,
                        ParamsMap.of("productList", products)
                )
        ));
        return field;
    }

    @SuppressWarnings("unused")
    public Table.PlainTextCell generateTotalCostField(InvoiceProduct invoiceProduct) {
        BigDecimal totalCost = invoiceProduct.getCost() == null || invoiceProduct.getCount() == null
                ? BigDecimal.ZERO
                : new BigDecimal(invoiceProduct.getCount()).multiply(invoiceProduct.getCost());
        return new Table.PlainTextCell(totalCost.toString());
    }

    @SuppressWarnings("unused")
    public LookupField generateShelfField(InvoiceProduct invoiceProduct) {
        final LookupField shelfField = componentsFactory.createComponent(LookupField.class);
        shelfField.setId("shelf");
        shelfField.setOptionsDatasource(shelvesDs);
        shelfField.setDatasource(getInvoiceProductDs(invoiceProduct), "shelf");
        shelfField.setWidth("100%");

        WebBusinessUtils.adjustShelfField(shelfField, invoiceProduct.getProduct());

        invoiceProductsDs.addItemPropertyChangeListener(e -> {
            if (e.getProperty().equals("rentPlaceType")) {
                WebBusinessUtils.adjustShelfField(shelfField, invoiceProduct.getProduct());
            }
        });

        return shelfField;
    }

    protected void onInvoiceTypeChanged(InvoiceType invoiceType) {
        setCaption();
        productsTableEditAction.setEnabled(invoiceType == InvoiceType.IN);
        productsTable.getColumn("prevCost").setCollapsed(invoiceType != InvoiceType.CHANGE_COST);
        productsTable.getColumn("cost").setCaption(invoiceType == InvoiceType.CHANGE_COST
                        ? getMessage("invoiceProduct.newCost")
                        : messages.getMessage("com.tvoyapolka.accounting.entity", "InvoiceProduct.cost")
        );
        productsTable.getColumn("totalCost").setCollapsed(invoiceType == InvoiceType.CHANGE_COST);
    }

    protected TextInputField.MaxLengthLimited createTextInputField(InvoiceProduct invoiceProduct,
                                                                   Class<? extends TextInputField.MaxLengthLimited> type,
                                                                   String property,
                                                                   boolean required,
                                                                   Integer maxTextLength,
                                                                   boolean enabled) {

        final TextInputField.MaxLengthLimited field = componentsFactory.createComponent(type);
        field.setId(property);
        field.setDatasource(getInvoiceProductDs(invoiceProduct), property);
        field.setRequired(required);
        field.setEnabled(enabled || isNew(invoiceProduct.getProduct()));
        field.setWidth("100%");
        if (maxTextLength != null) {
            field.setMaxLength(maxTextLength);
        }
        return field;
    }

    protected void setCaption() {
        final Invoice invoice = getItem();
        String invoiceTypeString = getMessage("caption." + invoice.getType().name());
        caption.setValue(formatMessage(
                "caption",
                invoiceTypeString,
                invoice.getContract().getNumber(),
                invoice.getContract().getContragent().getSystemName()
        ));
        setCaption(formatMessage("editorCaption", invoiceTypeString));
    }

    protected void createProductWithNewCost(InvoiceProduct invoiceProductOut) {
        final InvoiceProduct invoiceProductIn = metadata.create(InvoiceProduct.class);
        final Product oldProduct = invoiceProductOut.getProduct();
        final Product newProduct = metadata.create(Product.class);

        invoiceProductIn.setType(InvoiceProduct.InvoiceProductType.IN);
        invoiceProductIn.setInvoice(invoiceProductOut.getInvoice());
        invoiceProductIn.setCount(invoiceProductOut.getCount());
        invoiceProductIn.setPrevCost(invoiceProductOut.getPrevCost());
        invoiceProductIn.setCost(invoiceProductOut.getCost());
        invoiceProductIn.setProduct(newProduct);
        invoiceProductsDs.addItem(invoiceProductIn);

        newProduct.setName(oldProduct.getName());
        newProduct.setShop(oldProduct.getShop());
        newProduct.setContract(oldProduct.getContract());
        newProduct.setContragent(oldProduct.getContragent());
        newProduct.setDescription(oldProduct.getDescription());
        newProduct.setRentPlaceType(oldProduct.getRentPlaceType());
        newProduct.setShelf(oldProduct.getShelf());
        newProduct.setCost(invoiceProductOut.getCost());
        productDs.modified(newProduct);
    }

    @SuppressWarnings("unchecked")
    private Datasource<InvoiceProduct> getInvoiceProductDs(InvoiceProduct ip) {
        if (DS_MAP.containsKey(ip)) {
            return DS_MAP.get(ip);
        }

        Datasource<InvoiceProduct> ds = DsBuilder.create()
                .setAllowCommit(false)
                .setJavaClass(InvoiceProduct.class)
                .setRefreshMode(CollectionDatasource.RefreshMode.NEVER)
                .setViewName(View.LOCAL)
                .buildDatasource();

        ((DatasourceImplementation) ds).valid();
        DS_MAP.put(ip, ds);
        ds.setItem(ip);
        return ds;
    }

    private class TotalCostAggregationStrategy implements AggregationStrategy<InvoiceProduct, BigDecimal> {
        @Override
        public BigDecimal aggregate(Collection<InvoiceProduct> propertyValues) {
            return BusinessUtils.calculateInvoiceTotalCost(propertyValues);
        }

        @Override
        public Class<BigDecimal> getResultClass() {
            return BigDecimal.class;
        }
    }
}
