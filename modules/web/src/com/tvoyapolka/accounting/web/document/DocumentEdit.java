package com.tvoyapolka.accounting.web.document;

import com.haulmont.cuba.gui.components.AbstractEditor;
import com.tvoyapolka.accounting.entity.Document;

public class DocumentEdit extends AbstractEditor<Document> {
    @Override
    protected boolean preCommit() {
        Document document = getItem();
        document.setCode(document.getName());
        return true;
    }
}