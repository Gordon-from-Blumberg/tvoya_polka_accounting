package com.tvoyapolka.accounting.web.shelf;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 28.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.gui.components.AbstractEditor;
import com.tvoyapolka.accounting.entity.Shelf;

public class ShelfEdit extends AbstractEditor<Shelf> {
}
