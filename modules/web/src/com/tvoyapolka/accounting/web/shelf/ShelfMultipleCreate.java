package com.tvoyapolka.accounting.web.shelf;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 28.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.gui.components.AbstractLookup;
import com.haulmont.cuba.gui.components.TextField;

import javax.inject.Named;

public class ShelfMultipleCreate extends AbstractLookup {
    public static final String CREATE_ACTION = "create";
    public static final String CANCEL_ACTION = "cancel";

    @Named("from")
    protected TextField from;
    @Named("to")
    protected TextField to;

    public void create() {
        close(CREATE_ACTION);
    }

    public void cancel() {
        close(CANCEL_ACTION);
    }

    public Integer getFrom() {
        return Integer.parseInt(from.getRawValue());
    }

    public Integer getTo() {
        return Integer.parseInt(to.getRawValue());
    }
}
