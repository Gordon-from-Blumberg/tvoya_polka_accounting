package com.tvoyapolka.accounting.web.invoiceproduct;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 05.04.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.global.PersistenceHelper;
import com.haulmont.cuba.gui.WindowParam;
import com.haulmont.cuba.gui.WindowParams;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.data.Datasource;
import com.haulmont.cuba.gui.data.impl.AbstractDatasource;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.tvoyapolka.accounting.entity.*;
import com.tvoyapolka.accounting.utils.StringHelper;
import com.tvoyapolka.accounting.web.listener.ProductCostChangeListener;
import com.tvoyapolka.accounting.web.product.ProductListFrame;
import com.tvoyapolka.accounting.web.utils.WebBusinessUtils;
import com.tvoyapolka.accounting.web.utils.WebComponentUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class InvoiceProductCreate extends AbstractEditor<InvoiceProduct> {
    
    @Inject
    protected ComponentsFactory componentsFactory;
    @Inject
    protected Metadata metadata;

    @Named("invoiceProductDs")
    protected Datasource<InvoiceProduct> invoiceProductDs;
    @Named("productDs")
    protected AbstractDatasource<Product> productDs;
    @Named("productFieldGroup")
    protected FieldGroup productFieldGroup;
    @Named("productListFrame")
    protected ProductListFrame productListFrame;

    @WindowParam
    protected CollectionDatasource<RentPlaceType, UUID> rentPlaceTypesDs;
    @WindowParam
    protected CollectionDatasource<Shelf, UUID> shelvesDs;
    @WindowParam
    protected CollectionDatasource<Product, UUID> existingProductsDs;

    private TextInputField nameField;
    private TextInputField costField;

    @Override
    public void init(Map<String, Object> params) {
        WebComponentUtils.setFieldGroupTabIndexes(productFieldGroup);

        final InvoiceProduct invoiceProduct = WindowParams.ITEM.<InvoiceProduct>get(params);
        final InvoiceType invoiceType = invoiceProduct.getInvoice().getType();
        productDs.addItemChangeListener(e -> {
            if (e.getItem() != null) {
                final boolean isNew = PersistenceHelper.isNew(e.getItem());
                nameField.setEnabled(isNew);
                costField.setEnabled(isNew || invoiceType == InvoiceType.CHANGE_COST);
                getItem().setPrevCost(e.getItem().getCost());
            }
        });

        if (invoiceProduct.getInvoice().getContract().getDealType() == DealType.MARKUP) {
            final FieldGroup.FieldConfig baseCostField = productFieldGroup.getFieldNN("baseCost");
            baseCostField.setVisible(true);
            baseCostField.setRequired(true);

            final FieldGroup.FieldConfig markupField = productFieldGroup.getFieldNN("markup");
            markupField.setVisible(true);
            markupField.setRequired(true);

            productDs.addItemPropertyChangeListener(e -> {
                final Product product = e.getItem();
                if (e.getValue() != null && StringHelper.oneOf(e.getProperty(), "cost", "baseCost", "markup")) {
                    final BigDecimal value = (BigDecimal) e.getValue();
                    if ("cost".equals(e.getProperty()) && product.getBaseCost() != null) {
                        product.setMarkup(value.subtract(product.getBaseCost()));
                    }
                    if ("baseCost".equals(e.getProperty()) && product.getMarkup() != null) {
                        product.setCost(value.add(product.getMarkup()));
                    }
                    if ("markup".equals(e.getProperty()) && product.getBaseCost() != null) {
                        product.setCost(value.add(product.getBaseCost()));
                    }
                }
            });
        }
    }

    @Override
    protected void initNewItem(InvoiceProduct item) {
        if (item.getInvoice().getType() == InvoiceType.IN) {
            final Invoice invoice = item.getInvoice();
            final Product product = metadata.create(Product.class);
            product.setShop(invoice.getContract().getShop());
            product.setContract(invoice.getContract());
            product.setContragent(invoice.getContract().getContragent());
            item.setProduct(product);
            item.setType(InvoiceProduct.InvoiceProductType.IN);
        } else {
            item.setType(InvoiceProduct.InvoiceProductType.OUT);
        }
    }

    @Override
    protected void postInit() {
        final InvoiceProduct item = getItem();
        final InvoiceType invoiceType = item.getInvoice().getType();

        setValueToLookupField((LookupField) productFieldGroup.getFieldNN("rentPlaceType").getComponentNN());

        final LookupField shelfField = (LookupField) productFieldGroup.getFieldNN("shelf").getComponentNN();
        WebBusinessUtils.adjustShelfField(shelfField, item.getProduct());
        setValueToLookupField(shelfField);

        if (invoiceType == InvoiceType.IN) {
            ((HasValue) productFieldGroup.getFieldNN("cost").getComponentNN())
                    .addValueChangeListener(new ProductCostChangeListener(
                            existingProductsDs,
                            productListFrame::setProductList
                    ));
        } else {
            final List<Product> products = existingProductsDs.getItems()
                    .stream()
                    .filter(p -> p.getCount() > 0)
                    .collect(Collectors.toList());

            productListFrame.setProductList(products);
        }

        productListFrame.setProductSelectHandler(item::setProduct);

        if (invoiceType == InvoiceType.CHANGE_COST) {
            productFieldGroup.getFieldNN("prevCost").setVisible(true);
            productFieldGroup.getFieldNN("cost").setCaption(
                    messages.getMessage("com.tvoyapolka.accounting.web.invoice", "invoiceProduct.newCost")
            );
        }
    }

    @Override
    protected boolean preCommit() {
        final InvoiceProduct ip = getItem();
        final Product product = ip.getProduct();
        if (PersistenceHelper.isNew(product)) {
            product.setName(ip.getName());
            product.setCost(ip.getCost());
        } else {
            productDs.clearCommitLists();
            productDs.setItem(product);
        }
        product.setDescription(ip.getDescription());
        return true;
    }

    @SuppressWarnings("unused")
    public TextInputField generateNameField(Datasource ds, String fieldId) {
        nameField = createTextInputField(ds, fieldId, TextField.class, true, null);
        return nameField;
    }

    @SuppressWarnings("unused")
    public TextInputField generateCostField(Datasource ds, String fieldId) {
        costField = createTextInputField(ds, fieldId, TextField.class, true, null);
        return costField;
    }

    @SuppressWarnings("unused")
    public TextInputField generateDescriptionField(Datasource ds, String fieldId) {
        return createTextInputField(ds, fieldId, TextArea.class, false, 4);
    }

    @SuppressWarnings("unused")
    public LookupField generateRentPlaceTypeField(Datasource ds, String fieldId) {
        LookupField rentPlaceTypeField = componentsFactory.createComponent(LookupField.class);
        rentPlaceTypeField.setDatasource(ds, fieldId);
        rentPlaceTypeField.setOptionsDatasource(rentPlaceTypesDs);
        return rentPlaceTypeField;
    }

    @SuppressWarnings("unused")
    public LookupField generateShelfField(Datasource ds, String fieldId) {
        LookupField shelfField = componentsFactory.createComponent(LookupField.class);
        shelfField.setDatasource(ds, fieldId);
        shelfField.setOptionsDatasource(shelvesDs);
        productDs.addItemPropertyChangeListener(e -> {
            if (e.getProperty().equals("rentPlaceType")) {
                e.getItem().setShelf(null);
                WebBusinessUtils.adjustShelfField(shelfField, e.getItem());
            }
        });
        return shelfField;
    }

    private void setValueToLookupField(LookupField lookupField) {
        final CollectionDatasource ds = lookupField.getOptionsDatasource();
        if (ds != null && lookupField.isEnabled()) {
            if (ds.getState() != Datasource.State.VALID) {
                ds.refresh();
            }
            if (ds.size() == 1) {
                lookupField.setValue(ds.getItems().iterator().next());
            }
        }
    }

    private TextInputField createTextInputField(Datasource ds, String fieldId, Class<? extends TextInputField> type,
                                                boolean required, Integer rows) {

        final TextInputField field = componentsFactory.createComponent(type);
        field.setDatasource(ds, fieldId);
        field.setRequired(required);
        if (field instanceof TextArea && rows != null) {
            ((TextArea) field).setRows(rows);
        }
        return field;
    }
}
