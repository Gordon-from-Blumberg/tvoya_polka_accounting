package com.tvoyapolka.accounting.web.product;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 24.05.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.gui.WindowParam;
import com.haulmont.cuba.gui.components.AbstractFrame;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.GroupBoxLayout;
import com.haulmont.cuba.gui.components.OptionsGroup;
import com.tvoyapolka.accounting.entity.Product;
import com.tvoyapolka.accounting.utils.CollectionUtils;

import javax.inject.Named;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class ProductListFrame extends AbstractFrame {
    @Named("productListBox")
    protected GroupBoxLayout productListBox;
    @Named("addAsNewButton")
    protected Button addAsNewButton;
    @Named("productListGroup")
    protected OptionsGroup productListGroup;
    @Named("selectButton")
    protected Button selectButton;

    @WindowParam
    protected List<Product> productList;
    @WindowParam
    protected Consumer<Product> productSelectHandler;

    @Override
    public void init(Map<String, Object> params) {
        if (CollectionUtils.isNotEmpty(productList)) {
            productListGroup.setOptionsList(productList);
        }

        productListGroup.addValueChangeListener(e -> selectButton.setEnabled(e.getValue() != null));
    }

    public void addAsNew() {
        hide();
    }

    public void select() {
        productSelectHandler.accept( productListGroup.getValue() );
        hide();
    }

    public void hide() {
        productListBox.setVisible(false);
    }

    public void show() {
        productListBox.setVisible(true);
        selectButton.setEnabled( productListGroup.getValue() != null );
    }

    public void setProductSelectHandler(Consumer<Product> handler) {
        productSelectHandler = handler;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
        productListGroup.setOptionsList(productList);
        show();
    }
}
