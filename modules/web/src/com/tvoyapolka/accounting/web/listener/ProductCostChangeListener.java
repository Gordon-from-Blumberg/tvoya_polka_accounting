package com.tvoyapolka.accounting.web.listener;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 22.04.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.tvoyapolka.accounting.entity.Product;
import com.tvoyapolka.accounting.utils.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ProductCostChangeListener
        implements Component.ValueChangeListener {

    private final CollectionDatasource<Product, UUID> existingProductsDs;
    private final Consumer<List<Product>> handler;

    public ProductCostChangeListener(CollectionDatasource<Product, UUID> existingProductsDs,
                                     Consumer<List<Product>> handler) {
        this.existingProductsDs = existingProductsDs;
        this.handler = handler;
    }

    @Override
    public void valueChanged(Component.ValueChangeEvent e) {
        if (!CollectionUtils.isNotEmpty(existingProductsDs.getItems())) {
            return;
        }

        final BigDecimal cost = (BigDecimal) e.getValue();
        if (cost != null) {
            final List<Product> products = existingProductsDs.getItems()
                    .stream()
                    .filter(p -> cost.compareTo(p.getCost()) == 0)
                    .collect(Collectors.toList());

            if (!products.isEmpty()) {
                handler.accept(products);
            }
        }
    }
}
