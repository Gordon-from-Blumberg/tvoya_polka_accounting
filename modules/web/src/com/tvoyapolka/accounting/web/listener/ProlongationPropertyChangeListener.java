package com.tvoyapolka.accounting.web.listener;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 28.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.gui.data.Datasource;
import com.tvoyapolka.accounting.entity.Prolongation;
import com.tvoyapolka.accounting.service.ProlongationBusinessService;
import com.tvoyapolka.accounting.utils.StringHelper;

public class ProlongationPropertyChangeListener
        implements Datasource.ItemPropertyChangeListener<Prolongation> {

    private final ProlongationBusinessService prolongationBusinessService;

    public ProlongationPropertyChangeListener(ProlongationBusinessService prolongationBusinessService) {
        this.prolongationBusinessService = prolongationBusinessService;
    }

    @Override
    public void itemPropertyChanged(Datasource.ItemPropertyChangeEvent<Prolongation> e) {
        final String property = e.getProperty();
        final Prolongation prolongation = e.getItem();

        if (property.equals("rentPlaceType")) {
            if (prolongation.getRentPlaceType() != null
                    && prolongation.getRentPlaceType().getTariffes() != null
                    && prolongation.getRentPlaceType().getTariffes().size() == 1) {

                prolongation.setTariff(prolongation.getRentPlaceType().getTariffes().get(0));
            } else {
                prolongation.setTariff(null);
            }
        }
        if (StringHelper.oneOf(property, "duration", "tariff")) {
            prolongation.setCost(prolongationBusinessService.calculateCost(prolongation));
        }
        if (StringHelper.oneOf(property, "duration", "startDate")) {
            prolongation.setEndDate(prolongationBusinessService.calculateEndDate(prolongation));
        }
        if (property.equals("shelf") && prolongation.getShelf() != null) {
            prolongation.setTariff(prolongation.getShelf().getTariff());
        }
    }
}