package com.tvoyapolka.accounting.web.screens;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 02.04.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.gui.WindowParam;
import com.haulmont.cuba.gui.components.AbstractLookup;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.components.OptionsGroup;
import com.tvoyapolka.accounting.entity.PaymentType;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Map;

public class ContractPayment extends AbstractLookup {
    public static final String COMMIT_ACTION = "commit";
    public static final String CANCEL_ACTION = "cancel";

    @Named("label")
    protected Label label;
    @Named("paymentType")
    protected OptionsGroup optionsGroup;

    @WindowParam
    protected BigDecimal amount;

    @Override
    public void init(Map<String, Object> params) {
        label.setValue(String.format(getMessage("contractPayment.amountToPay"), amount));
        optionsGroup.setOptionsEnum(PaymentType.class);
        optionsGroup.setValue(PaymentType.CASH);
    }

    public void commit() {
        close(COMMIT_ACTION);
    }

    public void cancel() {
        close(CANCEL_ACTION);
    }

    public PaymentType getPaymentType() {
        return (PaymentType) optionsGroup.getValue();
    }
}
