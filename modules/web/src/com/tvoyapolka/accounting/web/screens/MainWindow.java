package com.tvoyapolka.accounting.web.screens;

import com.haulmont.cuba.core.global.Events;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.components.AbstractMainWindow;
import com.haulmont.cuba.gui.components.BoxLayout;
import com.haulmont.cuba.gui.components.Embedded;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.security.global.UserSession;
import com.haulmont.cuba.web.WebConfig;
import com.tvoyapolka.accounting.entity.Shop;
import com.tvoyapolka.accounting.web.events.CurrentShopChangedEvent;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.UUID;

public class MainWindow extends AbstractMainWindow {

    private static final String DIRECTOR_ID_ATTR = "directorId";
    private static final String SHOP_ID_ATTR = "shopId";

    @Inject
    private Embedded logoImage;
    @Inject
    private UserSessionSource userSessionSource;
    @Inject
    private WebConfig webConfig;
    @Inject
    private Events events;

    @Named("shopsDs")
    private CollectionDatasource<Shop, UUID> shopsDs;
    @Named("titleBar")
    private BoxLayout titleBar;
    @Named("currentShop")
    private LookupField currentShopLookupField;

    @Override
    public void init(Map<String, Object> params) {
        super.init(params);

        initLayoutAnalyzerContextMenu(logoImage);
        initLogoImage(logoImage);

        if (webConfig.getUseInverseHeader()) {
            titleBar.setStyleName("c-app-menubar c-inverse-header");
        }

        initCurrentShop(userSessionSource.getUserSession());
    }

    @Override
    public void ready() { }

    protected void initCurrentShop(UserSession userSession) {
        currentShopLookupField.addValueChangeListener(e -> {
            Shop selectedShop = (Shop) e.getValue();
            if (selectedShop != null) {
                userSessionSource.getUserSession().setAttribute(SHOP_ID_ATTR, selectedShop.getId());
            } else {
                userSessionSource.getUserSession().removeAttribute(SHOP_ID_ATTR);
            }

            events.publish(new CurrentShopChangedEvent(selectedShop));
        });

        final UUID directorIdAttribute = userSession.getAttribute(DIRECTOR_ID_ATTR);
        if (directorIdAttribute != null) {
            shopsDs.setQuery("select sh from tpa$Shop sh where sh.director.id = :session$directorId");
        }

        shopsDs.refresh();

        final UUID shopIdAttribute = userSession.getAttribute(SHOP_ID_ATTR);
        if (shopIdAttribute != null && shopsDs.containsItem(shopIdAttribute)) {
            currentShopLookupField.setValue(shopsDs.getItem(shopIdAttribute));
            currentShopLookupField.setEditable(false);
        } else if (shopsDs.size() > 0) {
            currentShopLookupField.setValue(shopsDs.getItems().iterator().next()); //todo check admin role
        }
    }
}