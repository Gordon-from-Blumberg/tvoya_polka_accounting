package com.tvoyapolka.accounting.web.screens;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 16.04.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.gui.WindowParam;
import com.haulmont.cuba.gui.components.AbstractLookup;
import com.haulmont.cuba.gui.components.OptionsGroup;
import com.tvoyapolka.accounting.entity.Product;

import javax.inject.Named;
import java.util.List;
import java.util.Map;

public class ExistingProductList extends AbstractLookup {

    @Named("productList")
    protected OptionsGroup productOptionsList;

    @WindowParam
    protected List<Product> productList;

    @Override
    public void init(Map<String, Object> params) {
        productOptionsList.setOptionsList(productList);
    }

    @SuppressWarnings("unused")
    public void addAsNew() {
        close("close");
    }
}
