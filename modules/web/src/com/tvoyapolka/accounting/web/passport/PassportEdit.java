package com.tvoyapolka.accounting.web.passport;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 25.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.gui.components.AbstractEditor;
import com.haulmont.cuba.gui.components.FieldGroup;
import com.tvoyapolka.accounting.entity.Passport;
import com.tvoyapolka.accounting.web.utils.WebComponentUtils;

import java.util.Map;

public class PassportEdit extends AbstractEditor<Passport> {
    @Override
    public void init(Map<String, Object> params) {
        WebComponentUtils.setFieldGroupTabIndexes((FieldGroup) getComponentNN("passportFieldGroup"), 30);
    }
}
