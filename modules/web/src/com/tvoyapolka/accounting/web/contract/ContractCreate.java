package com.tvoyapolka.accounting.web.contract;

import com.haulmont.bali.util.ParamsMap;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.actions.CreateAction;
import com.haulmont.cuba.gui.components.actions.EditAction;
import com.haulmont.cuba.gui.config.WindowConfig;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.data.Datasource;
import com.haulmont.cuba.gui.data.impl.AbstractDatasource;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.tvoyapolka.accounting.entity.*;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;
import com.tvoyapolka.accounting.service.ContractService;
import com.tvoyapolka.accounting.service.ProlongationBusinessService;
import com.tvoyapolka.accounting.service.ProlongationService;
import com.tvoyapolka.accounting.utils.DateUtils;
import com.tvoyapolka.accounting.utils.MapUtils;
import com.tvoyapolka.accounting.utils.StringHelper;
import com.tvoyapolka.accounting.web.events.CurrentShopChangedEvent;
import com.tvoyapolka.accounting.web.listener.ProlongationPropertyChangeListener;
import com.tvoyapolka.accounting.web.screens.ContractPayment;
import com.tvoyapolka.accounting.web.utils.WebBusinessUtils;
import com.tvoyapolka.accounting.web.utils.WebComponentUtils;
import org.springframework.context.event.EventListener;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.*;

import static com.tvoyapolka.accounting.utils.BusinessUtils.isShelfProlongation;

public class ContractCreate extends AbstractEditor<Contract> {
    protected static final List<String> ALWAYS_VISIBLE_FIELDS = Arrays.asList(
            "type", "dealType", "name", "phone", "inn", "comment"
    );

    protected static final Map<ContragentType, List<String>> VISIBLE_FIELDS_MAP = MapUtils
            .<ContragentType, List<String>>start()
            .add(ContragentType.SPECIAL, new ArrayList<>())
            .add(ContragentType.INDIVIDUAL, Arrays.asList(
                    "secondName", "lastName",
                    "postcode", "town", "street", "building", "subbuilding", "flat"
            ))
            .add(ContragentType.INDIVIDUAL_ENTREPRENEUR, Arrays.asList(
                    "ogrnIp", "postcode", "town", "street", "building", "subbuilding", "flat",
                    "legalPostcode", "legalTown", "legalStreet", "legalBuilding", "legalSubbuilding", "legalFlat"
            ))
            .add(ContragentType.LEGAL_ENTITY, Arrays.asList(
                    "postcode", "town", "street", "building", "subbuilding", "flat",
                    "ogrn", "kpp"
            ))
            .build();

    @Inject
    protected DataManager dataManager;
    @Inject
    protected UserSessionSource userSessionSource;
    @Inject
    protected Metadata metadata;
    @Inject
    protected ComponentsFactory componentsFactory;
    @Inject
    protected WindowConfig windowConfig;
    @Inject
    protected ContractService contractService;
    @Inject
    protected ProlongationService prolongationService;
    @Inject
    protected ProlongationBusinessService prolongationBusinessService;

    @Named("contractDs")
    protected Datasource<Contract> contractDs;
    @Named("prolongationsDs")
    protected Datasource<Prolongation> prolongationsDs;
    @Named("contragentDs")
    protected Datasource<Contragent> contragentDs;
    @Named("passportDs")
    protected AbstractDatasource<Passport> passportDs;
    @Named("shelvesDs")
    protected CollectionDatasource<Shelf, UUID> shelvesDs;
    @Named("archiveContragentsDs")
    protected CollectionDatasource<Contragent, UUID> archiveContragentsDs;

    @Named("contractCaption0")
    protected Label contractCaption0Label;
    @Named("contractCaption1")
    protected Label contractCaption1Label;
    @Named("directorLabel")
    protected Label directorLabel;
    @Named("shopLabel")
    protected Label shopLabel;
    @Named("contragentHeader")
    protected BoxLayout contragentHeader;
    @Named("contragentFieldGroup")
    protected FieldGroup contragentFieldGroup;
    @Named("editPassport")
    protected Button editPassportButton;
    @Named("prolongationsTable.create")
    protected CreateAction prolongationsTableCreateAction;
    @Named("prolongationsTable.edit")
    protected EditAction prolongationsTableEditAction;
    @Named("additionalTermsBox")
    protected GroupBoxLayout additionalTermsBox;

    private Log log = LogFactory.getLog(getClass());

    private boolean transactionConfirmed = false;
    private ContractCloseListener closeListener;

    @Override
    public void init(Map<String, Object> params) {
        closeListener = new ContractCloseListener();
        addCloseWithCommitListener(closeListener);

        contractDs.addItemPropertyChangeListener(e -> {
            if (e.getProperty().equals("dealType")) {
                final boolean isPercent = e.getItem().getDealType() == DealType.PERCENT;
                additionalTermsBox.setVisible(isPercent);
                if (isPercent && e.getItem().getPercent() == null) {
                    e.getItem().setPercent(new BigDecimal(30));
                }
            }
        });

        contragentDs.addItemPropertyChangeListener(e -> {
            final String property = e.getProperty();
            final Contragent contragent = e.getItem();

            if (property.equals("type")) {
                final ContragentType type = contragent.getType();
                editPassportButton.setVisible( type == ContragentType.INDIVIDUAL );
                adjustFields(type);

                if (type == ContragentType.SPECIAL) {
                    getItem().setDealType(DealType.MARKUP);
                    contragentFieldGroup.getFieldNN("dealType").setEditable(false);
                } else {
                    contragentFieldGroup.getFieldNN("dealType").setEditable(true);
                }

                if (type == ContragentType.INDIVIDUAL
                        || type == ContragentType.INDIVIDUAL_ENTREPRENEUR) {
                    contragentFieldGroup.getFieldNN("name").setCaption(getMessage("personalName"));
                } else {
                    contragentFieldGroup.getFieldNN("name").setCaption(getMessage("name"));
                }

            } else if (property.equals("name") && contragent.getPassport() != null) {
                contragent.getPassport().setFirstName(contragent.getName());
            } else if (property.equals("secondName") && contragent.getPassport() != null) {
                contragent.getPassport().setSecondName(contragent.getSecondName());
            } else if (property.equals("lastName") && contragent.getPassport() != null) {
                contragent.getPassport().setLastName(contragent.getLastName());
            }
        });

        prolongationsDs.addItemPropertyChangeListener(new ProlongationPropertyChangeListener(prolongationBusinessService));

        WebComponentUtils.setFieldGroupTabIndexes(contragentFieldGroup, 1, 12);

        archiveContragentsDs.refresh();

        prolongationsTableEditAction.setWindowId("tpa$Prolongation.create");
        prolongationsTableCreateAction.setWindowId("tpa$Prolongation.create");
        prolongationsTableCreateAction.setInitialValuesSupplier(() -> ParamsMap.of(
                "number",
                1 + getItem()
                        .getProlongations()
                        .stream()
                        .map(Prolongation::getNumber)
                        .max(Integer::compare)
                        .get()
        ));
        prolongationsTableEditAction.setWindowParams(ParamsMap.of("isNewContract", true));
        prolongationsTableCreateAction.setWindowParams(ParamsMap.of("isNewContract", true));
    }

    @Override
    protected void initNewItem(Contract item) {
        Shop currentShop = getCurrentShop();
        item.setShop(currentShop);
        item.setNumber(contractService.getNextContractNumber(currentShop));
        item.setDate(DateUtils.today());
        item.setClosed(false);
        item.setProlongations(new ArrayList<>());
        item.getProlongations().add(prolongationService.createNewProlongation(item));

        Contragent contragent = metadata.create(Contragent.class);
        contragent.setShop(currentShop);
        item.setContragent(contragent);

        Passport passport = metadata.create(Passport.class);
        contragent.setPassport(passport);
    }

    @Override
    protected void postInit() {
        final Contract contract = getItem();

        contractCaption0Label.setValue( getMessage("caption.contract0") );
        contractCaption1Label.setValue( getMessage("caption.contract1") );
        shopLabel.setValue( formatMessage("caption.shop", contract.getShop().getName()) );
        directorLabel.setValue( formatMessage("caption.director", contract.getShop().getDirector().getName()) );

        if (archiveContragentsDs.size() == 0) {
           getComponentNN("getFromArchiveButton").setEnabled(false);
        }

        adjustFields(contract.getContragent().getType());
    }

    @SuppressWarnings("unused")
    public LookupField generateShelfField(Prolongation prolongation) {
        final LookupField shelfField = componentsFactory.createComponent(LookupField.class);
        shelfField.setId("shelf");
        shelfField.setOptionsDatasource(shelvesDs);
        shelfField.setValue(prolongation.getShelf());
        shelfField.setWidth("100%");

        WebBusinessUtils.adjustShelfField(shelfField, prolongation);

        prolongationsDs.addItemPropertyChangeListener(e -> {
            if (e.getProperty().equals("rentPlaceType")) {
                WebBusinessUtils.adjustShelfField(shelfField, prolongation);
            }
        });

        shelfField.addValueChangeListener(e -> prolongation.setShelf((Shelf) e.getValue()));

        return shelfField;
    }

    @SuppressWarnings("unused")
    public CheckBox generatePromotionField(Prolongation prolongation) {
        final CheckBox promotionField = componentsFactory.createComponent(CheckBox.class);
        promotionField.setId("promotion");
        promotionField.setValue(prolongationBusinessService.isPromotionApplied(prolongation));

        prolongationsDs.addItemPropertyChangeListener(e -> {
            if (StringHelper.oneOf(e.getProperty(), "duration", "rentPlaceType")) {
                promotionField.setValue(prolongationBusinessService.isPromotionApplied(prolongation));
            }
        });

        return promotionField;
    }

    @SuppressWarnings("unused")
    @EventListener(CurrentShopChangedEvent.class)
    public void onShopChanged(CurrentShopChangedEvent event) {
        showNotification("Shop was changed! Current shop is " + event.toString());
    }

    @SuppressWarnings("unused")
    public void getFromArchive(Component source) {
        getItem().setContragent(null);
        Container fromArchiveButtonWrapper = (Container) getComponentNN("fromArchiveButtonWrapper");
        fromArchiveButtonWrapper.remove(source);
        getComponentNN("getFromArchiveLabel").setVisible(true);
        fromArchiveButtonWrapper.add(createArchiveContragentPicker());
        contragentHeader.setEnabled(false);
    }

    @SuppressWarnings({"unused", "unchecked"})
    public void editPassport(Component editPassportButton) {
        AbstractEditor<Passport> passportEditor = openEditor(
                "tpa$Passport.edit",
                getItem().getContragent().getPassport(),
                WindowManager.OpenType.DIALOG,
                passportDs
        );
        passportEditor.addCloseWithCommitListener(() -> {
            ((HasCaption) editPassportButton).setCaption(getMessage("editPassport"));

            final Passport passport = passportEditor.getItem();
            final Contragent contragent = getItem().getContragent();

            contragent.setName(passport.getFirstName());
            contragent.setSecondName(passport.getSecondName());
            contragent.setLastName(passport.getLastName());
        });
    }

    @Override
    protected boolean preCommit() {
        if (getItem().getDealType() == DealType.STANDARD
                && !transactionConfirmed) {
            ContractPayment window = (ContractPayment) openWindow(
                    "contractPayment",
                    WindowManager.OpenType.DIALOG,
                    ParamsMap.of("amount", calculateSummaryCost())
            );
            window.addCloseListener(action -> {
                if (ContractPayment.COMMIT_ACTION.equals(action)) {
                    transactionConfirmed = true;
                    closeListener.setPaymentType(window.getPaymentType());
                    commitAndClose();
                }
            });
            return false;
        }

        _preCommit();

        return true;
    }

    private void _preCommit() {
        final Contract contract = getItem();
        log.debug("_preCommit: adjust fields of new contract #%s", contract.getNumber());

        if (contract.getContragent().getType() != ContragentType.INDIVIDUAL) {
            contract.getContragent().setPassport(null);
            passportDs.clearCommitLists();
        }

        final List<String> visibleFields = VISIBLE_FIELDS_MAP.get(contract.getContragent().getType());
        contragentFieldGroup.getFields().forEach(fc -> {
            final String id = fc.getId();
            if (!ALWAYS_VISIBLE_FIELDS.contains(id) && !visibleFields.contains(id)) {
                ((HasValue) fc.getComponentNN()).setValue(null);
            }
        });

        Date endDate = null;
        for (Prolongation prolongation : contract.getProlongations()) {
            if (!isShelfProlongation(prolongation)) {
                prolongation.setShelf(null);
            } else {
                prolongation.getShelf().setState(ShelfState.RENTED);
                prolongation.getShelf().setActiveContract(contract);
                shelvesDs.modifyItem(prolongation.getShelf());
            }

            if (endDate == null || endDate.before(prolongation.getEndDate())) {
                endDate = prolongation.getEndDate();
            }
        }
        if (contract.getDealType() == DealType.STANDARD && endDate != null) {
            contract.setEndDate(endDate);
        }

        final boolean active = endDate != null && DateUtils.afterToday(endDate)
                || contract.getDealType() != DealType.STANDARD;
        contract.getContragent().setActive(active);
        contract.setClosed(!active);

        if (active) {
            contract.getContragent().setActiveContract(contract);
        }

        if (contract.getDealType() != DealType.PERCENT) {
            contract.setPercent(null);
        }
    }

    private void commitTransaction(PaymentType paymentType) {
        final ContractTransaction transaction = metadata.create(ContractTransaction.class);
        transaction.setDirection(TransactionDirection.IN);
        transaction.setPaymentType(paymentType);
        transaction.setDate(DateUtils.today());
        transaction.setContract(getItem());
        transaction.setValue(calculateSummaryCost());
        dataManager.commit(transaction);
    }

    private void adjustFields(ContragentType contragentType) {
        final List<String> visibleFields = VISIBLE_FIELDS_MAP.get(contragentType);

        contragentFieldGroup.getFields().forEach(fc -> {
            final String id = fc.getId();
            fc.setVisible(ALWAYS_VISIBLE_FIELDS.contains(id) || visibleFields.contains(id));
        });
    }

    private Shop getCurrentShop() {
        UUID shopId = userSessionSource.getUserSession().getAttribute("shopId");

        if (shopId == null) {
            throw new RuntimeException(getMessage("error.shopIsNotSelected"));
        }

        return dataManager.load(Shop.class)
                    .view("shop.newContract")
                    .id(shopId)
                    .one();
    }

    private BigDecimal calculateSummaryCost() {
        return getItem().getProlongations()
                .stream()
                .map(Prolongation::getCost)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    @SuppressWarnings("unchecked")
    private LookupPickerField createArchiveContragentPicker() {
        final LookupPickerField archiveContragentPicker = componentsFactory.createComponent(LookupPickerField.class);
        archiveContragentPicker.setOptionsDatasource(archiveContragentsDs);
        archiveContragentPicker.addValueChangeListener(e -> {
            //getItem().setContragent((Contragent) e.getValue());
            contragentDs.setItem((Contragent) e.getValue());
            contragentHeader.setEnabled(true);
        });
        archiveContragentPicker.addAction(new ArchiveContragentsClearAction(archiveContragentPicker));
        return archiveContragentPicker;
    }

    private class ArchiveContragentsClearAction extends PickerField.ClearAction {
        ArchiveContragentsClearAction(PickerField pickerField) {
            super(pickerField);
        }

        @Override
        public void actionPerform(Component component) {
            Contract contract = getItem();
            contract.setContragent(metadata.create(Contragent.class));
            contract.getContragent().setShop(contract.getShop());
            final Contragent contragent = metadata.create(Contragent.class);
            contragent.setShop(getItem().getShop());
            contragentDs.setItem(contragent);
        }
    }

    private class ContractCloseListener implements CloseWithCommitListener {
        private PaymentType paymentType;

        @Override
        public void windowClosedWithCommitAction() {
            if (getItem().getDealType() == DealType.STANDARD) {
                commitTransaction(paymentType);
            }
            Invoice invoice = metadata.create(Invoice.class);
            invoice.setContract(getItem());
            getWindowManager().openEditor(
                    windowConfig.getWindowInfo("tpa$Invoice.create"),
                    invoice,
                    WindowManager.OpenType.THIS_TAB,
                    ParamsMap.of("invoiceType", InvoiceType.IN)
            );
        }

        public void setPaymentType(PaymentType paymentType) {
            this.paymentType = paymentType;
        }
    }
}
