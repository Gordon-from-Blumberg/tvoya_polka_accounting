package com.tvoyapolka.accounting.web.prolongation;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 27.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.PersistenceHelper;
import com.haulmont.cuba.gui.WindowParam;
import com.haulmont.cuba.gui.components.AbstractEditor;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.components.LookupField;
import com.haulmont.cuba.gui.data.Datasource;
import com.tvoyapolka.accounting.entity.Contract;
import com.tvoyapolka.accounting.entity.Prolongation;
import com.tvoyapolka.accounting.entity.Shop;
import com.tvoyapolka.accounting.service.ProlongationBusinessService;
import com.tvoyapolka.accounting.utils.BusinessUtils;
import com.tvoyapolka.accounting.utils.DateUtils;
import com.tvoyapolka.accounting.utils.StringHelper;
import com.tvoyapolka.accounting.web.listener.ProlongationPropertyChangeListener;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Map;

public class ProlongationCreate extends AbstractEditor<Prolongation> {

    @Inject
    protected ProlongationBusinessService prolongationBusinessService;

    @Named("prolongationDs")
    protected Datasource<Prolongation> prolongationDs;
    @Named("shopDs")
    protected Datasource<Shop> shopDs;
    @Named("headerLabel")
    protected Label headerLabel;
    @Named("promotionLabel")
    protected Label promotionLabel;
    @Named("shelfField")
    protected LookupField shelfField;

    @WindowParam
    protected boolean isNewContract;

    @Override
    public void init(Map<String, Object> params) {
        prolongationDs.addItemPropertyChangeListener(new ProlongationPropertyChangeListener(prolongationBusinessService));
        prolongationDs.addItemPropertyChangeListener(e -> {
            if (StringHelper.oneOf(e.getProperty(), "duration", "rentPlaceType")) {
                promotionLabel.setVisible(prolongationBusinessService.isPromotionApplied(e.getItem()));
            }
            if (e.getProperty().equals("rentPlaceType")) {
                adjustShelfField(getItem());
            }
        });
    }

    @Override
    protected void initNewItem(Prolongation item) {
        shopDs.setItem(item.getContract().getShop());
        item.setDate(DateUtils.today());

        final List<Prolongation> prolongations = item.getContract().getProlongations();
        final Prolongation lastProlongation = prolongations != null && !prolongations.isEmpty()
                ? prolongations.get(prolongations.size() - 1)
                : null;

        if (lastProlongation != null) {
            if (PersistenceHelper.isNew(lastProlongation)) {
                item.setStartDate(lastProlongation.getStartDate());
                item.setDuration(lastProlongation.getDuration());
                item.setEndDate(lastProlongation.getEndDate());
            } else if (lastProlongation.getEndDate() != null) {
                item.setStartDate( DateUtils.nextDay(lastProlongation.getEndDate()) );
            }
        }
    }

    @Override
    protected void postInit() {
        final Prolongation item = getItem();
        final Contract contract = item.getContract();
        headerLabel.setValue(isNewContract
                ? String.format(getMessage("header.newContract"),
                    item.getNumber(), contract.getNumber(), DateUtils.format(contract.getDate()))
                : String.format(getMessage("header.prolongation"),
                    contract.getNumber(), DateUtils.format(contract.getDate()))
        );

        promotionLabel.setVisible(prolongationBusinessService.isPromotionApplied(item));

        adjustShelfField(item);
    }

    private void adjustShelfField(Prolongation prolongation) {
        final boolean isShelf = BusinessUtils.isShelfProlongation(prolongation);
        shelfField.setEnabled(isShelf);
        shelfField.setRequired(isShelf);
    }
}