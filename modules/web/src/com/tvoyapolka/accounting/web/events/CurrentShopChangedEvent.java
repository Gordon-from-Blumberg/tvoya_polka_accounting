package com.tvoyapolka.accounting.web.events;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 14.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.gui.events.UiEvent;
import com.tvoyapolka.accounting.entity.Shop;
import org.springframework.context.ApplicationEvent;

public class CurrentShopChangedEvent extends ApplicationEvent implements UiEvent {
    private Shop newShop;

    public CurrentShopChangedEvent(Shop source) {
        super(source);

        newShop = source;
    }

    public Shop getNewShop() {
        return newShop;
    }

    public void setNewShop(Shop newShop) {
        this.newShop = newShop;
    }

    @Override
    public String toString() {
        return newShop != null ? newShop.getName() : "null";
    }
}
