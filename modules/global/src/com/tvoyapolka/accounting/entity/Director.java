package com.tvoyapolka.accounting.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.EmbeddedParameters;
import com.haulmont.cuba.core.entity.annotation.Listeners;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.Group;

import javax.persistence.*;
import java.util.Set;

@Listeners("tpa_DirectorEntityListener")
@NamePattern("#getName|tpUser")
@Table(name = "TPA_DIRECTOR")
@Entity(name = "tpa$Director")
public class Director extends StandardEntity {
    private static final long serialVersionUID = 6035965735454738719L;

    @OnDelete(DeletePolicy.UNLINK)
    @OnDeleteInverse(DeletePolicy.CASCADE)
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TP_USER_ID", unique = true)
    protected TPUser tpUser;

    @OrderBy("createTs")
    @OnDelete(DeletePolicy.DENY)
    @OneToMany(mappedBy = "director", cascade = CascadeType.PERSIST)
    protected Set<Shop> shops;

    @OnDelete(DeletePolicy.CASCADE)
    @OnDeleteInverse(DeletePolicy.DENY)
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "GROUP_ID", unique = true)
    protected Group group;

    @Column(name = "OGRN_IP", length = 20)
    protected String ogrnIp;

    @Embedded
    @EmbeddedParameters(nullAllowed = false)
    @AttributeOverrides({
        @AttributeOverride(name = "postcode", column = @Column(name = "LEGAL_ADDRESS_POSTCODE")),
        @AttributeOverride(name = "town", column = @Column(name = "LEGAL_ADDRESS_TOWN")),
        @AttributeOverride(name = "street", column = @Column(name = "LEGAL_ADDRESS_STREET")),
        @AttributeOverride(name = "building", column = @Column(name = "LEGAL_ADDRESS_BUILDING")),
        @AttributeOverride(name = "subbuilding", column = @Column(name = "LEGAL_ADDRESS_SUBBUILDING")),
        @AttributeOverride(name = "flat", column = @Column(name = "LEGAL_ADDRESS_FLAT"))
    })
    protected Address legalAddress = new Address();

    public TPUser getTpUser() {
        return tpUser;
    }

    public void setTpUser(TPUser tpUser) {
        this.tpUser = tpUser;
    }

    public Set<Shop> getShops() {
        return shops;
    }

    public void setShops(Set<Shop> shops) {
        this.shops = shops;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getOgrnIp() {
        return ogrnIp;
    }

    public void setOgrnIp(String ogrnIp) {
        this.ogrnIp = ogrnIp;
    }

    public Address getLegalAddress() {
        return legalAddress;
    }

    public void setLegalAddress(Address legalAddress) {
        this.legalAddress = legalAddress;
    }

    public String getName() {
        return tpUser.getName();
    }
}