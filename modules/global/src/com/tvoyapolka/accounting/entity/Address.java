package com.tvoyapolka.accounting.entity;

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.EmbeddableEntity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@NamePattern("%s, %s, %s|town,street,building")
@MetaClass(name = "tpa$Address")
@Embeddable
public class Address extends EmbeddableEntity {
    private static final long serialVersionUID = 5431476161866667776L;

    public Address() {}

    public Address(String postcode,
                   String town,
                   String street,
                   String building,
                   String subbuilding,
                   String flat) {
        this.postcode = postcode;
        this.town = town;
        this.street = street;
        this.building = building;
        this.subbuilding = subbuilding;
        this.flat = flat;
    }

    @Column(name = "POSTCODE", length = 16)
    protected String postcode;

    @Column(name = "TOWN", length = 64)
    protected String town;

    @Column(name = "STREET", length = 64)
    protected String street;

    @Column(name = "BUILDING", length = 8)
    protected String building;

    @Column(name = "SUBBUILDING", length = 8)
    protected String subbuilding;

    @Column(name = "FLAT", length = 8)
    protected String flat;

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getTown() {
        return town;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return street;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getBuilding() {
        return building;
    }

    public void setSubbuilding(String subbuilding) {
        this.subbuilding = subbuilding;
    }

    public String getSubbuilding() {
        return subbuilding;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getFlat() {
        return flat;
    }
}