package com.tvoyapolka.accounting.entity;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 03.04.19
 * <p>
 * $Id$
 */

import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Listeners;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.tvoyapolka.accounting.utils.DateUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Listeners("tpa_InvoiceEntityListener")
@NamePattern("#_getName|type,contract,dateTime")
@Table(name = "TPA_INVOICE")
@Entity(name = "tpa$Invoice")
public class Invoice extends StandardEntity {
    private static final long serialVersionUID = 937312421457929112L;

    @OnDelete(DeletePolicy.DENY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTRACT_ID")
    protected Contract contract;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_TIME", nullable = false)
    protected Date dateTime;

    @OrderBy("product.name")
    @Composition
    @OneToMany(mappedBy = "invoice")
    protected List<InvoiceProduct> products;

    @NotNull
    @Column(name = "TYPE", nullable = false)
    protected Integer type;

    @Column(name = "TOTAL_COST")
    protected BigDecimal totalCost;

    @Column(name = "ORDER_COST")
    protected BigDecimal orderCost;

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public List<InvoiceProduct> getProducts() {
        return products;
    }

    public void setProducts(List<InvoiceProduct> products) {
        this.products = products;
    }

    public InvoiceType getType() {
        return type == null ? null : InvoiceType.fromId(type);
    }

    public void setType(InvoiceType type) {
        this.type = type == null ? null : type.getId();
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public BigDecimal getOrderCost() {
        return orderCost;
    }

    public void setOrderCost(BigDecimal orderCost) {
        this.orderCost = orderCost;
    }

    public String _getName() {
        String label;
        switch (getType()) {
            case IN:
                label = "Прием";
                break;
            case OUT:
                label = "Выдача";
                break;
            case CHANGE_COST:
                label = "Изменение цены";
                break;
            default:
                throw new NullPointerException();
        }

        return label + String.format(" %s по договору №%s",
                DateUtils.format(dateTime), contract.getNumber());
    }
}
