package com.tvoyapolka.accounting.entity;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 06.03.19
 * <p>
 * $Id$
 */

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;

public enum ShelfState implements EnumClass<Integer> {

    FREE(10),
    BOOKED(20),
    RENTED(30);

    private Integer id;

    ShelfState(Integer value) {
        this.id = value;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Nullable
    public static ShelfState fromId(Integer id) {
        for (ShelfState at : ShelfState.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
