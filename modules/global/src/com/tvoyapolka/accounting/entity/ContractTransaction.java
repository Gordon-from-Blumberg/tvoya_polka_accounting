package com.tvoyapolka.accounting.entity;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 02.04.19
 * <p>
 * $Id$
 */

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.tvoyapolka.accounting.utils.DateUtils;

import javax.persistence.*;

@NamePattern("#getName|direction,date,value,contract")
@Table(name = "TPA_CONTRACT_TRANSACTION")
@Entity(name = "tpa$ContractTransaction")
public class ContractTransaction extends AbstractTransaction {
    private static final long serialVersionUID = 3609473477157455175L;

    @OnDelete(DeletePolicy.DENY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTRACT_ID")
    protected Contract contract;

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public String getName() {
        return String.format("%s от %s на сумму %s по договору №%s",
                TransactionDirection.IN.getId().equals(direction) ? "Оплата аренды" : "Выдача",
                DateUtils.format(date),
                value,
                contract.getNumber()
        );
    }
}