package com.tvoyapolka.accounting.entity;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 06.03.19
 * <p>
 * $Id$
 */

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@NamePattern("#getName|number,state")
@Table(name = "TPA_SHELF", uniqueConstraints = {
        @UniqueConstraint(name = "IDX_TPA_SHELF_ON_SHOP_NUMBER_UNQ", columnNames = {"SHOP_ID", "NUMBER_"})
})
@Entity(name = "tpa$Shelf")
public class Shelf extends StandardEntity {
    private static final long serialVersionUID = 64644158821062430L;

    @OnDelete(DeletePolicy.UNLINK)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SHOP_ID")
    protected Shop shop;

    @NotNull
    @Column(name = "NUMBER_", nullable = false)
    protected Integer number;

    @NotNull
    @Column(name = "STATE", nullable = false)
    protected Integer state = 10;

    @OnDelete(DeletePolicy.DENY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTIVE_CONTRACT_ID")
    protected Contract activeContract;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TARIFF_ID")
    protected Tariff tariff;

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public ShelfState getState() {
        return state == null ? null : ShelfState.fromId(state);
    }

    public void setState(ShelfState state) {
        this.state = state == null ? null : state.getId();
    }

    public Contract getActiveContract() {
        return activeContract;
    }

    public void setActiveContract(Contract activeContract) {
        this.activeContract = activeContract;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public String getName() {
        String stateStatus;
        switch (getState()) {
            case FREE:
                stateStatus = "СВОБОДНА";
                break;
            case BOOKED:
                stateStatus = "ЗАБРОНИРОВАНА";
                break;
            case RENTED:
                stateStatus = "ЗАНЯТА";
                break;
            default:
                stateStatus = "СВОБОДНА";
        }
        return String.format("Полка №%s [%s]", number, stateStatus);
    }
}
