package com.tvoyapolka.accounting.entity;

import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.EmbeddedParameters;
import com.haulmont.cuba.core.entity.annotation.Listeners;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.Group;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Listeners("tpa_ShopEntityListener")
@NamePattern("#getName|director, address")
@Table(name = "TPA_SHOP")
@Entity(name = "tpa$Shop")
public class Shop extends StandardEntity {
    private static final long serialVersionUID = 5537180663643521445L;

    @Embedded
    @EmbeddedParameters(nullAllowed = false)
    @AttributeOverrides({
        @AttributeOverride(name = "postcode", column = @Column(name = "ADDRESS_POSTCODE")),
        @AttributeOverride(name = "town", column = @Column(name = "ADDRESS_TOWN")),
        @AttributeOverride(name = "street", column = @Column(name = "ADDRESS_STREET")),
        @AttributeOverride(name = "building", column = @Column(name = "ADDRESS_BUILDING")),
        @AttributeOverride(name = "subbuilding", column = @Column(name = "ADDRESS_SUBBUILDING")),
        @AttributeOverride(name = "flat", column = @Column(name = "ADDRESS_FLAT"))
    })
    protected Address address = new Address();

    @NotNull
    @Column(name = "WORK_PHONE", nullable = false, length = 20)
    protected String workPhone;

    @OnDelete(DeletePolicy.UNLINK)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DIRECTOR_ID")
    protected Director director;

    @OrderBy("name")
    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToMany(mappedBy = "shop", cascade = CascadeType.PERSIST)
    protected List<RentPlaceType> rentPlaceTypes;

    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OnDeleteInverse(DeletePolicy.DENY)
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUP_ID", unique = true)
    protected Group group;

    @Composition
    @OrderBy("number")
    @OneToMany(mappedBy = "shop")
    protected List<Shelf> shelves;

    @NotNull
    @Column(name = "ARTICUL_BASE", nullable = false, unique = true)
    protected Long articulBase;

    public void setRentPlaceTypes(List<RentPlaceType> rentPlaceTypes) {
        this.rentPlaceTypes = rentPlaceTypes;
    }

    public List<RentPlaceType> getRentPlaceTypes() {
        return rentPlaceTypes;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public Director getDirector() {
        return director;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Shelf> getShelves() {
        return shelves;
    }

    public void setShelves(List<Shelf> shelves) {
        this.shelves = shelves;
    }

    public Long getArticulBase() {
        return articulBase;
    }

    public void setArticulBase(Long articulBase) {
        this.articulBase = articulBase;
    }

    public String getName() {
        return String.format("%s, %s, %s (%s)",
                address.getTown(), address.getStreet(), address.getBuilding(), director.getName());
    }
}