package com.tvoyapolka.accounting.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum ContragentType implements EnumClass<Integer> {
    SPECIAL(0),
    INDIVIDUAL(10),
    INDIVIDUAL_ENTREPRENEUR(20),
    LEGAL_ENTITY(30);

    private Integer id;

    ContragentType(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static ContragentType fromId(Integer id) {
        for (ContragentType at : ContragentType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}