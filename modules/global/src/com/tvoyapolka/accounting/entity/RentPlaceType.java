package com.tvoyapolka.accounting.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@NamePattern("%s|name")
@Table(name = "TPA_RENT_PLACE_TYPE", uniqueConstraints = {
    @UniqueConstraint(name = "IDX_TPA_RENT_PLACE_TYPE_ON_SHOP_NAME_UNQ", columnNames = {"SHOP_ID", "NAME"})
})
@Entity(name = "tpa$RentPlaceType")
public class RentPlaceType extends StandardEntity {
    private static final long serialVersionUID = -3140105529248707897L;

    @OnDelete(DeletePolicy.UNLINK)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SHOP_ID")
    protected Shop shop;

    @NotNull
    @Column(name = "NAME", nullable = false, length = 64)
    protected String name;

    @OrderBy("cost")
    @JoinTable(name = "TPA_RENT_PLACE_TYPE_TARIFF_LINK",
        joinColumns = @JoinColumn(name = "RENT_PLACE_TYPE_ID"),
        inverseJoinColumns = @JoinColumn(name = "TARIFF_ID"))
    @OnDelete(DeletePolicy.UNLINK)
    @OnDeleteInverse(DeletePolicy.UNLINK)
    @ManyToMany
    protected List<Tariff> tariffes;

    public void setTariffes(List<Tariff> tariffes) {
        this.tariffes = tariffes;
    }

    public List<Tariff> getTariffes() {
        return tariffes;
    }


    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Shop getShop() {
        return shop;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}