package com.tvoyapolka.accounting.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.EmbeddedParameters;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.haulmont.cuba.security.entity.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@NamePattern("%s|user")
@Table(name = "TPA_TP_USER")
@Entity(name = "tpa$TPUser")
public class TPUser extends StandardEntity {
    private static final long serialVersionUID = 7359462676840593941L;

    @NotNull
    @OnDeleteInverse(DeletePolicy.CASCADE)
    @OnDelete(DeletePolicy.CASCADE)
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "USER_ID", unique = true)
    protected User user;

    @NotNull
    @Column(name = "PHONE", nullable = false, length = 20)
    protected String phone;

    @Embedded
    @EmbeddedParameters(nullAllowed = false)
    @AttributeOverrides({
        @AttributeOverride(name = "postcode", column = @Column(name = "ADDRESS_POSTCODE")),
        @AttributeOverride(name = "town", column = @Column(name = "ADDRESS_TOWN")),
        @AttributeOverride(name = "street", column = @Column(name = "ADDRESS_STREET")),
        @AttributeOverride(name = "building", column = @Column(name = "ADDRESS_BUILDING")),
        @AttributeOverride(name = "subbuilding", column = @Column(name = "ADDRESS_SUBBUILDING")),
        @AttributeOverride(name = "flat", column = @Column(name = "ADDRESS_FLAT"))
    })
    protected Address address;

    @Column(name = "INN", length = 20)
    protected String inn;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getName() {
        return user.getName();
    }
}