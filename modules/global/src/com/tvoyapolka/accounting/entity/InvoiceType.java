package com.tvoyapolka.accounting.entity;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 03.04.19
 * <p>
 * $Id$
 */

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;

public enum InvoiceType implements EnumClass<Integer> {

    IN(10),
    OUT(20),
    CHANGE_COST(30);
    
    private Integer id;

    InvoiceType(Integer value) {
        this.id = value;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Nullable
    public static InvoiceType fromId(Integer id) {
        for (InvoiceType at : InvoiceType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
