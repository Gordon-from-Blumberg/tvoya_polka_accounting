package com.tvoyapolka.accounting.entity;

import com.haulmont.chile.core.annotations.NumberFormat;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.EmbeddedParameters;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Table(name = "TPA_PASSPORT", uniqueConstraints = {
    @UniqueConstraint(name = "IDX_TPA_PASSPORT_ON_SERIES_NUMBER_UNQ", columnNames = {"SERIES", "NUMBER_"})
})
@Entity(name = "tpa$Passport")
public class Passport extends StandardEntity {
    private static final long serialVersionUID = -3481603334254701577L;

    @NotNull
    @Column(name = "FIRST_NAME", nullable = false)
    protected String firstName;

    @Column(name = "SECOND_NAME")
    protected String secondName;

    @NotNull
    @Column(name = "LAST_NAME", nullable = false)
    protected String lastName;

    @NumberFormat(pattern = "0000")
    @NotNull
    @Column(name = "SERIES", nullable = false)
    protected Integer series;

    @NumberFormat(pattern = "000000")
    @NotNull
    @Column(name = "NUMBER_", nullable = false)
    protected Integer number;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "DATE_OF_ISSUE", nullable = false)
    protected Date dateOfIssue;

    @NotNull
    @Column(name = "DEPARTMENT_CODE", nullable = false, length = 10)
    protected String departmentCode;

    @Embedded
    @EmbeddedParameters(nullAllowed = false)
    @AttributeOverrides({
        @AttributeOverride(name = "postcode", column = @Column(name = "REGISTRY_ADDRESS_POSTCODE")),
        @AttributeOverride(name = "town", column = @Column(name = "REGISTRY_ADDRESS_TOWN")),
        @AttributeOverride(name = "street", column = @Column(name = "REGISTRY_ADDRESS_STREET")),
        @AttributeOverride(name = "building", column = @Column(name = "REGISTRY_ADDRESS_BUILDING")),
        @AttributeOverride(name = "subbuilding", column = @Column(name = "REGISTRY_ADDRESS_SUBBUILDING")),
        @AttributeOverride(name = "flat", column = @Column(name = "REGISTRY_ADDRESS_FLAT"))
    })
    protected Address registryAddress;

    public void setDateOfIssue(Date dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public Date getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setRegistryAddress(Address registryAddress) {
        this.registryAddress = registryAddress;
    }

    public Address getRegistryAddress() {
        return registryAddress;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setSeries(Integer series) {
        this.series = series;
    }

    public Integer getSeries() {
        return series;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }
}