package com.tvoyapolka.accounting.entity;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 02.04.19
 * <p>
 * $Id$
 */

import com.haulmont.chile.core.annotations.MetaClass;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.UnavailableInSecurityConstraints;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@MappedSuperclass
@MetaClass(name = "tpa$AbstractTransaction")
@UnavailableInSecurityConstraints
public abstract class AbstractTransaction extends StandardEntity {
    private static final long serialVersionUID = 194881139442583406L;

    @NotNull
    @Column(name = "DIRECTION", nullable = false)
    protected Integer direction;

    @NotNull
    @Column(name = "PAYMENT_TYPE", nullable = false)
    protected Integer paymentType;

    @NotNull
    @Column(name = "DATE", nullable = false)
    protected Date date;

    @NotNull
    @Column(name = "VALUE", nullable = false)
    protected BigDecimal value;

    public TransactionDirection getDirection() {
        return direction != null ? TransactionDirection.fromId(direction) : null;
    }

    public void setDirection(TransactionDirection direction) {
        this.direction = direction != null ? direction.getId() : null;
    }

    public PaymentType getPaymentType() {
        return paymentType != null ? PaymentType.fromId(paymentType) : null;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType != null ? paymentType.getId() : null;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}