package com.tvoyapolka.accounting.entity;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 03.04.19
 * <p>
 * $Id$
 */

import com.haulmont.chile.core.annotations.MetaProperty;
import com.haulmont.chile.core.datatypes.impl.EnumClass;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.Listeners;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Listeners("tpa_InvoiceProductEntityListener")
@Table(name = "TPA_INVOICE_PRODUCT")
@Entity(name = "tpa$InvoiceProduct")
public class InvoiceProduct extends StandardEntity {
    private static final long serialVersionUID = 7176823391638465583L;

    @OnDelete(DeletePolicy.UNLINK)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVOICE_ID")
    protected Invoice invoice;

    @OnDelete(DeletePolicy.UNLINK)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID")
    protected Product product;

    @NotNull
    @Column(name = "COUNT", nullable = false)
    protected Integer count;

    @Column(name = "PREV_COST")
    protected BigDecimal prevCost;

    @Column(name = "COST")
    protected BigDecimal cost;

    @NotNull
    @Column(name = "TYPE", nullable = false)
    protected Integer type;

    @Transient
    @MetaProperty
    protected String name;

    @Transient
    @MetaProperty
    protected String description;

    @Transient
    @MetaProperty
    protected RentPlaceType rentPlaceType;

    @Transient
    @MetaProperty
    protected Shelf shelf;

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
        if (product != null) {
            name = product.getName();
            description = product.getDescription();
            rentPlaceType = product.getRentPlaceType();
            shelf = product.getShelf();
        }
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getPrevCost() {
        return prevCost;
    }

    public void setPrevCost(BigDecimal prevCost) {
        this.prevCost = prevCost;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getName() {
        return product != null ? product.getName() : null;
    }

    public void setName(String name) {
        if (product != null) {
            this.name = name;
            product.setName(name);
        }
    }

    public String getDescription() {
        return product != null ? product.getDescription() : null;
    }

    public void setDescription(String description) {
        if (product != null) {
            this.description = description;
            product.setDescription(description);
        }
    }

    public RentPlaceType getRentPlaceType() {
        return product != null ? product.getRentPlaceType() : null;
    }

    public void setRentPlaceType(RentPlaceType rentPlaceType) {
        if (product != null) {
            this.rentPlaceType = rentPlaceType;
            product.setRentPlaceType(rentPlaceType);
        }
    }

    public Shelf getShelf() {
        return product != null ? product.getShelf() : null;
    }

    public void setShelf(Shelf shelf) {
        if (product != null) {
            this.shelf = shelf;
            product.setShelf(shelf);
        }
    }

    public InvoiceProductType getType() {
        return type == null ? null : InvoiceProductType.fromId(type);
    }

    public void setType(InvoiceProductType type) {
        this.type = type == null ? null : type.getId();
    }

    public enum InvoiceProductType implements EnumClass<Integer> {
        IN(10),
        OUT(20);

        private Integer id;

        InvoiceProductType(Integer value) {
            this.id = value;
        }

        @Override
        public Integer getId() {
            return id;
        }

        @Nullable
        public static InvoiceProductType fromId(Integer id) {
            for (InvoiceProductType at : InvoiceProductType.values()) {
                if (at.getId().equals(id)) {
                    return at;
                }
            }
            return null;
        }
    }
}
