package com.tvoyapolka.accounting.entity;

import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.EmbeddedParameters;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.tvoyapolka.accounting.utils.StringHelper;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static org.apache.commons.lang.StringUtils.isNotBlank;

@NamePattern("#getSystemName|type,name,secondName,lastName")
@Table(name = "TPA_CONTRAGENT")
@Entity(name = "tpa$Contragent")
public class Contragent extends StandardEntity {
    private static final long serialVersionUID = 756523879170622392L;

    @NotNull
    @Column(name = "NAME", nullable = false)
    protected String name;

    @Column(name = "SECOND_NAME", length = 32)
    protected String secondName;

    @Column(name = "LAST_NAME", length = 32)
    protected String lastName;

    @OnDelete(DeletePolicy.UNLINK)
    @OnDeleteInverse(DeletePolicy.DENY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SHOP_ID")
    protected Shop shop;

    @OnDelete(DeletePolicy.DENY)
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTIVE_CONTRACT_ID")
    protected Contract activeContract;

    @Embedded
    @EmbeddedParameters(nullAllowed = false)
    @AttributeOverrides({
        @AttributeOverride(name = "postcode", column = @Column(name = "ADDRESS_POSTCODE")),
        @AttributeOverride(name = "town", column = @Column(name = "ADDRESS_TOWN")),
        @AttributeOverride(name = "street", column = @Column(name = "ADDRESS_STREET")),
        @AttributeOverride(name = "building", column = @Column(name = "ADDRESS_BUILDING")),
        @AttributeOverride(name = "subbuilding", column = @Column(name = "ADDRESS_SUBBUILDING")),
        @AttributeOverride(name = "flat", column = @Column(name = "ADDRESS_FLAT"))
    })
    protected Address address;

    @Embedded
    @EmbeddedParameters(nullAllowed = false)
    @AttributeOverrides({
            @AttributeOverride(name = "postcode", column = @Column(name = "LEGAL_ADDRESS_POSTCODE")),
            @AttributeOverride(name = "town", column = @Column(name = "LEGAL_ADDRESS_TOWN")),
            @AttributeOverride(name = "street", column = @Column(name = "LEGAL_ADDRESS_STREET")),
            @AttributeOverride(name = "building", column = @Column(name = "LEGAL_ADDRESS_BUILDING")),
            @AttributeOverride(name = "subbuilding", column = @Column(name = "LEGAL_ADDRESS_SUBBUILDING")),
            @AttributeOverride(name = "flat", column = @Column(name = "LEGAL_ADDRESS_FLAT"))
    })
    protected Address legalAddress;

    @Column(name = "PHONE", length = 20)
    protected String phone;

    @NotNull
    @Column(name = "TYPE_", nullable = false)
    protected Integer type = 10;

    @OnDelete(DeletePolicy.CASCADE)
    @Composition
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PASSPORT_ID")
    protected Passport passport;

    @Column(name = "INN", length = 20)
    protected String inn;

    @Column(name = "OGRN_IP", length = 20)
    protected String ogrnIp;

    @Column(name = "OGRN", length = 20)
    protected String ogrn;

    @Column(name = "KPP", length = 20)
    protected String kpp;

    @NotNull
    @Column(name = "ACTIVE", nullable = false)
    protected Boolean active;

    @Column(name = "COMMENT")
    protected String comment;

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Shop getShop() {
        return shop;
    }

    public Contract getActiveContract() {
        return activeContract;
    }

    public void setActiveContract(Contract activeContract) {
        this.activeContract = activeContract;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public Address getLegalAddress() {
        return legalAddress;
    }

    public void setLegalAddress(Address legalAddress) {
        this.legalAddress = legalAddress;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setType(ContragentType type) {
        this.type = type == null ? null : type.getId();
    }

    public ContragentType getType() {
        return type == null ? null : ContragentType.fromId(type);
    }

    public Passport getPassport() {
        return passport;
    }

    public void setPassport(Passport passport) {
        this.passport = passport;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getInn() {
        return inn;
    }

    public String getOgrnIp() {
        return ogrnIp;
    }

    public void setOgrnIp(String ogrnIp) {
        this.ogrnIp = ogrnIp;
    }

    public String getOgrn() {
        return ogrn;
    }

    public void setOgrn(String ogrn) {
        this.ogrn = ogrn;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSystemName() {
        if (ContragentType.INDIVIDUAL.getId().equals(type)) {
            String result = isNotBlank(lastName) ? lastName : "";
            String _name = isNotBlank(name) ? StringHelper.getFirstUpper(name) + "." : "";
            if (isNotBlank(result) && isNotBlank(_name)) {
                result += " " + _name;
            }
            String _secondName = isNotBlank(secondName) ? StringHelper.getFirstUpper(secondName) + "." : "";
            if (isNotBlank(result) && isNotBlank(_secondName)) {
                result += " " + _secondName;
            }
            return result;
        } else {
            return name;
        }
    }
}