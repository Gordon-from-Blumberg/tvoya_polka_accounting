package com.tvoyapolka.accounting.entity;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 06.03.19
 * <p>
 * $Id$
 */

import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import com.tvoyapolka.accounting.utils.DateUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@NamePattern("#getName|number,date")
@Table(name = "TPA_CONTRACT", uniqueConstraints = {
        @UniqueConstraint(name = "IDX_TPA_CONTRACT_ON_SHOP_NUMBER_UNQ", columnNames = {"SHOP_ID", "NUMBER_"})
})
@Entity(name = "tpa$Contract")
public class Contract extends StandardEntity {
    private static final long serialVersionUID = 6569389666614565221L;

    @OnDelete(DeletePolicy.UNLINK)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SHOP_ID")
    protected Shop shop;

    @OnDelete(DeletePolicy.DENY)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTRAGENT_ID")
    protected Contragent contragent;

    @NotNull
    @Column(name = "NUMBER_", nullable = false)
    protected Integer number;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_", nullable = false)
    protected Date date;

    @Temporal(TemporalType.DATE)
    @Column(name = "END_DATE")
    protected Date endDate;

    @OrderBy("number")
    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToMany(mappedBy = "contract", cascade = CascadeType.PERSIST)
    protected List<Prolongation> prolongations;

    @Column(name = "CLOSED")
    protected Boolean closed = false;

    @NotNull
    @Column(name = "DEAL_TYPE", nullable = false)
    protected Integer dealType = 10;

    @Column(name = "PERCENT")
    protected BigDecimal percent;

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Contragent getContragent() {
        return contragent;
    }

    public void setContragent(Contragent contragent) {
        this.contragent = contragent;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Prolongation> getProlongations() {
        return prolongations;
    }

    public void setProlongations(List<Prolongation> prolongations) {
        this.prolongations = prolongations;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public void setDealType(DealType dealType) {
        this.dealType = dealType == null ? null : dealType.getId();
    }

    public DealType getDealType() {
        return dealType == null ? null : DealType.fromId(dealType);
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

    public String getName() {
        return String.format("Договор №%s от %s", number, DateUtils.format(date));
    }
}
