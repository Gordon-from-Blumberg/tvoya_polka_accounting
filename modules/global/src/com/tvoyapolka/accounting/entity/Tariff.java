package com.tvoyapolka.accounting.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@NamePattern("#getName|cost")
@Table(name = "TPA_TARIFF")
@Entity(name = "tpa$Tariff")
public class Tariff extends StandardEntity {
    private static final long serialVersionUID = -5014577914891902725L;

    @NotNull
    @Column(name = "COST", nullable = false, unique = true)
    protected BigDecimal cost;

    public BigDecimal getCost() {
        return cost;
    }

    public String getName() {
        return "Тариф " + cost.longValue(); //todo
    }
}