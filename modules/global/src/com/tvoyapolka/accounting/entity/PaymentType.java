package com.tvoyapolka.accounting.entity;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 02.04.19
 * <p>
 * $Id$
 */

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;

public enum PaymentType implements EnumClass<Integer> {
    CASH(10),
    CASHLESS(20);

    private Integer id;

    PaymentType(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Nullable
    public static PaymentType fromId(Integer id) {
        for (PaymentType at : PaymentType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}