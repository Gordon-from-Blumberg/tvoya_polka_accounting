package com.tvoyapolka.accounting.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum DealType implements EnumClass<Integer> {

    STANDARD(10),
    PERCENT(20),
    MARKUP(30);

    private Integer id;

    DealType(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static DealType fromId(Integer id) {
        for (DealType at : DealType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}