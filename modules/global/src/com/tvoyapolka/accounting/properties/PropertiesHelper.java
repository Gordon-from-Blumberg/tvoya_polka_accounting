package com.tvoyapolka.accounting.properties;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 21.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.sys.AppContext;

public class PropertiesHelper {
    private static Boolean isDev = null;
    private PropertiesHelper() {}

    public static boolean isDevEnvironment() {
        if (isDev == null) {
            isDev = getBoolean("tpa.dev");
        }
        return isDev;
    }

    public static boolean getBoolean(String key) {
        return Boolean.valueOf(AppContext.getProperty(key));
    }
}