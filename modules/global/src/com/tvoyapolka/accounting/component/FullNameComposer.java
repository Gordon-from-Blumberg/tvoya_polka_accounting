package com.tvoyapolka.accounting.component;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 01.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.security.entity.User;
import com.tvoyapolka.accounting.utils.StringHelper;

public interface FullNameComposer {
    String FULL_NAME_PATTERN_PROPERTY_NAME = "cuba.user.fullNamePattern";
    String DEFAULT_PATTERN = "{LL| }{F|. }{M|.}";

    String DELIMITER = "|";
    String FIRST_NAME_PATTERN = "F";
    String MIDDLE_NAME_PATTERN = "M";
    String LAST_NAME_PATTERN = "L";
    String REGEX_SECTION = "\\{(.+?)\\}";
    String REGEX_PLACEHOLDER = String.format(
            "([%s%s%s]{1,2})\\%s",
            FIRST_NAME_PATTERN,
            MIDDLE_NAME_PATTERN,
            LAST_NAME_PATTERN,
            DELIMITER
    );

    String composeFullName(User user);

    enum NamePlaceholder {
        F {
            @Override
            String getName(User user) {
                return getFirstUpper(user.getFirstName());
            }
        },
        FF {
            @Override
            String getName(User user) {
                return emptyIfNull(user.getFirstName());
            }
        },
        M {
            @Override
            String getName(User user) {
                return getFirstUpper(user.getMiddleName());
            }
        },
        MM {
            @Override
            String getName(User user) {
                return emptyIfNull(user.getMiddleName());
            }
        },
        L {
            @Override
            String getName(User user) {
                return getFirstUpper(user.getLastName());
            }
        },
        LL {
            @Override
            String getName(User user) {
                return emptyIfNull(user.getLastName());
            }
        };

        abstract String getName(User user);

        protected String getFirstUpper(String string) {
            return StringHelper.getFirstUpper(string);
        }

        protected String emptyIfNull(String string) {
            return string != null ? string : "";
        }
    }
}