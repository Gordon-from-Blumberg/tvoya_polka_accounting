package com.tvoyapolka.accounting.constants;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 18.03.19
 * <p>
 * $Id$
 */

public interface Roles {
    String DEFAULT_ROLE = "Default";
    String ADMINISTRATORS_ROLE = "Administrators";
    String DIRECTOR_ROLE = "Director";
    String SELLER_ROLE = "Seller";
}