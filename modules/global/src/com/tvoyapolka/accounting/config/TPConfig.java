package com.tvoyapolka.accounting.config;

import com.haulmont.cuba.core.config.Config;
import com.haulmont.cuba.core.config.Property;
import com.haulmont.cuba.core.config.Source;
import com.haulmont.cuba.core.config.SourceType;

@Source(type = SourceType.DATABASE)
public interface TPConfig extends Config {
    @Property("tpa.defaultGroup")
    String getDefaultGroup();
}