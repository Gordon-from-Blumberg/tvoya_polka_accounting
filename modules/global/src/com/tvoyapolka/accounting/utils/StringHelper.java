package com.tvoyapolka.accounting.utils;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 28.03.19
 * <p>
 * $Id$
 */

public class StringHelper {
    private StringHelper() {}

    public static boolean oneOf(String value, String... strings) {
        if (value == null) return false;

        for (String str : strings) {
            if (value.equals(str)) return true;
        }

        return false;
    }

    public static String getFirstUpper(String input) {
        String trimmed = input != null ? input.trim() : "";
        return trimmed.length() > 0 ? trimmed.substring(0, 1).toUpperCase() : trimmed;
    }
}