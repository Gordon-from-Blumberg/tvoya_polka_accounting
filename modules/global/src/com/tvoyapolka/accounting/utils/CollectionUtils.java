package com.tvoyapolka.accounting.utils;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 18.03.19
 * <p>
 * $Id$
 */

import java.util.Collection;
import java.util.function.Predicate;

public class CollectionUtils {
    private CollectionUtils() {}

    public static <T> T find(Collection<T> collection, Predicate<T> predicate) {
        for (T item : collection)
            if (predicate.test(item))
                return item;

        return null;
    }

    public static <T> int count(Collection<T> collection, Predicate<T> predicate) {
        int count = 0;
        for (T item : collection)
            if (predicate.test(item))
                count++;

        return count;
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return collection != null && !collection.isEmpty();
    }
}
