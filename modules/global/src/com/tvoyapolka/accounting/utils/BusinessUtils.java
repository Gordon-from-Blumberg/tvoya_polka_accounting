package com.tvoyapolka.accounting.utils;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 26.03.19
 * <p>
 * $Id$
 */

import com.tvoyapolka.accounting.entity.InvoiceProduct;
import com.tvoyapolka.accounting.entity.Product;
import com.tvoyapolka.accounting.entity.Prolongation;
import com.tvoyapolka.accounting.service.ShopInitService;

import java.math.BigDecimal;
import java.util.Collection;

public class BusinessUtils {
    private BusinessUtils() {}

    public static boolean isShelfProlongation(Prolongation prolongation) {
        return prolongation.getRentPlaceType() != null
                && ShopInitService.SHELF.equalsIgnoreCase(prolongation.getRentPlaceType().getName());
    }
    
    public static boolean isShelfProduct(Product product) {
        return product.getRentPlaceType() != null
                && ShopInitService.SHELF.equalsIgnoreCase(product.getRentPlaceType().getName());
    }

    public static BigDecimal calculateInvoiceTotalCost(Collection<InvoiceProduct> invoiceProducts) {
        return invoiceProducts
                .stream()
                .map(ip -> ip.getCost() != null && ip.getCount() != null
                        ? new BigDecimal(ip.getCount()).multiply(ip.getCost())
                        : BigDecimal.ZERO)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }
}
