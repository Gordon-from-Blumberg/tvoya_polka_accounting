package com.tvoyapolka.accounting.utils;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 01.03.19
 * <p>
 * $Id$
 */

import org.apache.commons.lang.StringUtils;

import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Replacer {
    private final Pattern regexPattern;
    private final UnaryOperator<String> replacer;

    private Replacer(String regex, UnaryOperator<String> replacer) {
        this.regexPattern = Pattern.compile(regex);
        this.replacer = replacer;
    }

    public static String replace(String input, String regex, UnaryOperator<String> replacer, boolean emptyToEmpty) {
        return _replace(input, Pattern.compile(regex), replacer, emptyToEmpty);
    }

    public static String replace(String input, String regex, UnaryOperator<String> replacer) {
        return _replace(input, Pattern.compile(regex), replacer, false);
    }

    public static Replacer createReplacer(String regex, UnaryOperator<String> replacer) {
        return new Replacer(regex, replacer);
    }

    public String replace(String input) {
        return _replace(input, regexPattern, replacer, false);
    }

    private static String _replace(String input, Pattern regexPattern, UnaryOperator<String> replacer, boolean emptyToEmpty) {
        final Matcher m = regexPattern.matcher(input);
        final StringBuffer sb = new StringBuffer();

        while (m.find()) {
            final String replacement = replacer.apply(m.group(1));
            if (emptyToEmpty && StringUtils.isBlank(replacement)) {
                return "";
            }
            m.appendReplacement(sb, replacer.apply(m.group(1)));
        }
        m.appendTail(sb);

        return sb.toString();
    }
}
