package com.tvoyapolka.accounting.utils;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 24.01.19
 * <p>
 * $Id$
 */

import java.util.HashMap;
import java.util.Map;

public class MapUtils {
    public static <K, V> MapBuilder<K, V> start() {
        return new MapBuilder<>();
    }

    public static <K, V> Map<K, V> of(K key, V value) {
        final Map<K, V> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    public static class MapBuilder<K, V> {
        private final Map<K, V> map = new HashMap<>();

        public MapBuilder<K, V> add(K key, V value) {
            map.put(key, value);
            return this;
        }

        public Map<K, V> build() {
            return map;
        }
    }
}
