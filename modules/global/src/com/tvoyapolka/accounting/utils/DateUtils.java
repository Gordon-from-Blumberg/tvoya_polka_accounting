package com.tvoyapolka.accounting.utils;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 11.03.19
 * <p>
 * $Id$
 */

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    private DateUtils() {}

    public static Date today() {
        return new Date();
    }

    public static Date tomorrow() {
        return org.apache.commons.lang3.time.DateUtils.addDays(new Date(), 1);
    }

    public static String format(Date date) {
        return DATE_FORMAT.format(date);
    }

    public static Date addDays(Date date, int days) {
        return org.apache.commons.lang3.time.DateUtils.addDays(date, days);
    }

    public static Date addWeeks(Date date, int weeks) {
        return org.apache.commons.lang3.time.DateUtils.addWeeks(date, weeks);
    }

    public static boolean afterToday(Date date) {
        return date != null && date.after(today());
    }

    public static Date nextDay(Date date) {
        return addDays(date, 1);
    }
}
