package com.tvoyapolka.accounting.utils;

import java.util.Random;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 11.03.19
 * <p>
 * $Id$
 */

public class PasswordGenerator {
    private static final String PASSWORD_SYMBOLS = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz23456789";
    private static final int PASSWORD_LENGTH = 8;

    private PasswordGenerator() {}

    public static String generate() {
        Random rand = new Random();
        String password = "";
        for (int i = 0; i < PASSWORD_LENGTH; i++) {
            password += PASSWORD_SYMBOLS.charAt(rand.nextInt(PASSWORD_SYMBOLS.length()));
        }
        return password;
    }
}
