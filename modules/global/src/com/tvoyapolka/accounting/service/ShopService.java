package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 19.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.security.entity.Group;
import com.tvoyapolka.accounting.entity.Shop;

public interface ShopService {
    String NAME = "tvoya_polka_accounting_ShopService";

    Shop findShopByGroup(Group group);
}
