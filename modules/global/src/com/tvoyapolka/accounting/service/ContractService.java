package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 11.03.19
 * <p>
 * $Id$
 */

import com.tvoyapolka.accounting.entity.Shop;

public interface ContractService {
    String NAME = "tvoya_polka_accounting_ContractService";

    Integer getNextContractNumber(Shop shop);
}
