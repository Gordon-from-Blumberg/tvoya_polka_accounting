package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 16.03.19
 * <p>
 * $Id$
 */

import com.tvoyapolka.accounting.entity.Prolongation;

import java.math.BigDecimal;
import java.util.Date;

public interface ProlongationBusinessService {
    String NAME = "tvoya_polka_accounting_ProlongationCostService";

    BigDecimal calculateCost(Prolongation prolongation);

    boolean isPromotionApplied(Prolongation prolongation);

    Date calculateEndDate(Prolongation prolongation);
}