package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 11.03.19
 * <p>
 * $Id$
 */

import com.tvoyapolka.accounting.entity.Contract;
import com.tvoyapolka.accounting.entity.Prolongation;

public interface ProlongationService {
    String NAME = "tvoya_polka_accounting_ProlongationService";

    Prolongation createNewProlongation(Contract contract);
}
