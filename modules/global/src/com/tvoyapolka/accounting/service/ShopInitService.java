package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 16.03.19
 * <p>
 * $Id$
 */

import com.tvoyapolka.accounting.entity.RentPlaceType;
import com.tvoyapolka.accounting.entity.Shop;

import java.util.List;

public interface ShopInitService {
    String NAME = "tvoya_polka_accounting_ShopInitService";

    String SHELF = "Полка";
    String HANGER = "Вешалка";
    String HALL_PLACE = "Место в зале";

    Long ARTICUL_BASE_STEP = 1000000000L;

    List<RentPlaceType> createStandardRentPlaceTypeList(Shop shop);

    Long getNextArticulBase();
}