package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 01.02.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.security.entity.Group;

public interface GroupService {
    String NAME = "tvoya_polka_accounting_GroupService";

    Group getDefaultGroup();
}
