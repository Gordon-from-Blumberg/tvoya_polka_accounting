package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 20.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.EmailException;

public interface TPEmailService {
    String NAME = "tvoya_polka_accounting_TPEmailService";

    void sendPasswordEmail(String to, String login, String password) throws EmailException;

    void sendAppStartedEmail();
}
