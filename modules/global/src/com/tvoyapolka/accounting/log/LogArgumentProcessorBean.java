package com.tvoyapolka.accounting.log;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 26.02.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.entity.Role;
import com.haulmont.cuba.security.entity.User;
import com.tvoyapolka.accounting.entity.Director;
import com.tvoyapolka.accounting.entity.Product;
import com.tvoyapolka.accounting.entity.Shop;
import com.tvoyapolka.accounting.entity.TPUser;

public class LogArgumentProcessorBean implements LogArgumentProcessor {
    @Override
    public String process(Object argument) {
        if (argument == null) {
            return "null";
        }

        if (argument instanceof StandardEntity) {
            if (argument instanceof User) return ((User) argument).getName();
            if (argument instanceof TPUser) return ((TPUser) argument).getName();
            if (argument instanceof Group) return ((Group) argument).getName();
            if (argument instanceof Director) return ((Director) argument).getName();
            if (argument instanceof Shop) return ((Shop) argument).getName();
            if (argument instanceof Role) return ((Role) argument).getName();
            if (argument instanceof Product) return productToString((Product) argument);
        }

        return argument.toString();
    }

    protected String productToString(Product product) {
        return String.format("%s (%sр)", product.getName(), product.getCost());
    }
}
