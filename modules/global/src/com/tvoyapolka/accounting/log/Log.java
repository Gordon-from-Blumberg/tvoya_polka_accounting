package com.tvoyapolka.accounting.log;

import org.slf4j.Logger;

import static java.lang.String.format;

public class Log {
    private final Logger logger;
    private final LogArgumentProcessor processor;
    
    Log(Logger logger, LogArgumentProcessor processor) {
        this.logger = logger;
        this.processor = processor;
    }
    
    public void debug(String template, Object... args) {
        logger.debug(format(template, process(args)));
    }
    
    public void debug(Throwable th, String template, Object... args) {
        logger.debug(format(template, process(args)), th);
    }

    public void info(String template, Object... args) {
        logger.info(format(template, process(args)));
    }

    public void info(Throwable th, String template, Object... args) {
        logger.info(format(template, process(args)), th);
    }

    public void warn(String template, Object... args) {
        logger.warn(format(template, process(args)));
    }

    public void warn(Throwable th, String template, Object... args) {
        logger.warn(format(template, process(args)), th);
    }

    public void error(String template, Object... args) {
        logger.error(format(template, process(args)));
    }

    public void error(Throwable th, String template, Object... args) {
        logger.error(format(template, process(args)), th);
    }
    
    private String[] process(Object... args) {
        if (args == null || args.length == 0) {
            return new String[0];
        }
        String[] processedArgs = new String[args.length];

        for (int i = 0; i < args.length; i++) {
            processedArgs[i] = processor.process(args[i]);
        }

        return processedArgs;
    }
}