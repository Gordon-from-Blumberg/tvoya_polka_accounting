package com.tvoyapolka.accounting.log;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 26.02.19
 * <p>
 * $Id$
 */

public interface LogArgumentProcessor {
    String process(Object argument);
}
