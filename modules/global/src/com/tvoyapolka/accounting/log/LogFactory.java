package com.tvoyapolka.accounting.log;

import org.slf4j.LoggerFactory;

public class LogFactory {
    private static final LogArgumentProcessor PROCESSOR = new LogArgumentProcessorBean();
    public static Log getLog(Class<?> type) {
        return new Log(LoggerFactory.getLogger(type), PROCESSOR);
    }
}