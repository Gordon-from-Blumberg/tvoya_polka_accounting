package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 19.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.security.entity.Group;
import com.tvoyapolka.accounting.entity.Shop;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(ShopService.NAME)
public class ShopServiceBean implements ShopService {

    @Inject
    protected DataManager dataManager;

    @Override
    public Shop findShopByGroup(Group group) {
        return dataManager.load(Shop.class)
                .view("shop.withGroup")
                .query("select sh from tpa$Shop sh where sh.group.id = :groupId")
                .parameter("groupId", group.getId())
                .optional()
                .orElse(null);
    }
}
