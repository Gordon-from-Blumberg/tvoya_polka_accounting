package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 16.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.global.View;
import com.tvoyapolka.accounting.entity.RentPlaceType;
import com.tvoyapolka.accounting.entity.Shop;
import com.tvoyapolka.accounting.entity.Tariff;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service(ShopInitService.NAME)
public class ShopInitServiceBean implements ShopInitService {

    //todo move to config
    private static final BigDecimal[] SHELF_TARIFFES = {
            new BigDecimal(200),
            new BigDecimal(250),
            new BigDecimal(300),
            new BigDecimal(350)
    };
    private static final BigDecimal[] HANGER_TARIFF = { new BigDecimal(50) };
    private static final BigDecimal[] HALL_PLACE_TARIFFES = { new BigDecimal(50), new BigDecimal(100) };

    @Inject
    private Metadata metadata;
    @Inject
    private DataManager dataManager;

    @Override
    public List<RentPlaceType> createStandardRentPlaceTypeList(Shop shop) {
        final List<Tariff> tariffList = dataManager.loadList(LoadContext.create(Tariff.class)
                .setView(View.LOCAL));

        final List<RentPlaceType> standardRentPlaceTypeList = new ArrayList<>(3);

        standardRentPlaceTypeList.add(create(shop, SHELF, tariffList, SHELF_TARIFFES));

        standardRentPlaceTypeList.add(create(shop, HANGER, tariffList, HANGER_TARIFF));

        standardRentPlaceTypeList.add(create(shop, HALL_PLACE, tariffList, HALL_PLACE_TARIFFES));

        return standardRentPlaceTypeList;
    }

    @Override
    public Long getNextArticulBase() {
        Long maxArticulBase = dataManager.loadValue(
                "select max(sh.articulBase) from tpa$Shop sh",
                Long.class
        )
                .one();

        if (maxArticulBase == null) {
            maxArticulBase = 0L; //todo get value from config
        }

        return maxArticulBase + ARTICUL_BASE_STEP;
    }

    private RentPlaceType create(Shop shop, String name, List<Tariff> tariffList, BigDecimal... availableTariffes) {
        final RentPlaceType rentPlaceType = metadata.create(RentPlaceType.class);
        rentPlaceType.setShop(shop);
        rentPlaceType.setName(name);
        rentPlaceType.setTariffes(filterTariffes(tariffList, availableTariffes));
        return rentPlaceType;
    }

    private List<Tariff> filterTariffes(List<Tariff> tariffList, BigDecimal... availableTariffes) {
        return tariffList
                .stream()
                .filter(t -> {
                    for (BigDecimal availableCost : availableTariffes) {
                        if (t.getCost().compareTo(availableCost) == 0) {
                            return true;
                        }
                    }
                    return false;
                })
                .collect(Collectors.toList());
    }
}