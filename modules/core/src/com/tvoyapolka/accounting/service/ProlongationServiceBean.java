package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 11.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.DataManager;
import com.tvoyapolka.accounting.entity.Contract;
import com.tvoyapolka.accounting.entity.Prolongation;
import com.tvoyapolka.accounting.utils.DateUtils;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(ProlongationService.NAME)
public class ProlongationServiceBean implements ProlongationService {

    @Inject
    private DataManager dataManager;

    @Override
    public Prolongation createNewProlongation(Contract contract) {
        final Prolongation prolongation = dataManager.create(Prolongation.class);
        prolongation.setContract(contract);
        prolongation.setNumber(0);
        prolongation.setDate(DateUtils.today());
        prolongation.setStartDate(DateUtils.tomorrow());

        return prolongation;
    }
}
