package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 01.02.19
 * <p>
 * $Id$
 */

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.security.entity.Group;

import com.tvoyapolka.accounting.config.TPConfig;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;

@Service(GroupService.NAME)
public class GroupServiceBean implements GroupService {

    @Inject
    private DataManager dataManager;
    @Inject
    private TPConfig tpConfig;

    private Log log = LogFactory.getLog(getClass());

    @Override
    public Group getDefaultGroup() {
        final String name = tpConfig.getDefaultGroup();
        LoadContext<Group> ctx = LoadContext.create(Group.class);
        ctx.setQueryString(String.format("select g from sec$Group g where g.name = '%s'", name));
        ctx.setView("_minimal");
        Group group = dataManager.load(ctx);
        log.debug("loaded default group %s by name %s", group, name);
        return group;
    }
}
