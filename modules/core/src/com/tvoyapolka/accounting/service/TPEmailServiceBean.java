package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 20.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.app.EmailerAPI;
import com.haulmont.cuba.core.global.EmailException;
import com.haulmont.cuba.core.global.EmailInfo;
import com.haulmont.cuba.core.global.Messages;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Date;

@Service(TPEmailService.NAME)
public class TPEmailServiceBean implements TPEmailService {

    @Inject
    protected EmailerAPI emailer;
    @Inject
    protected Messages messages;

    @Override
    public void sendPasswordEmail(String to, String login, String password) throws EmailException {
        emailer.sendEmail(
                to,
                getMessage("email.password.caption"),
                String.format(getMessage("email.password.bodyTemplate"), login, password),
                EmailInfo.TEXT_CONTENT_TYPE
        );
    }

    @Override
    public void sendAppStartedEmail() {
        emailer.sendEmailAsync(new EmailInfo(
                        getSystemToAddresses(),
                        getMessage("email.appStarted.caption"),
                        String.format(getMessage("email.appStarted.bodyTemplate"), new Date()),
                        EmailInfo.HTML_CONTENT_TYPE)
        );
    }

    private String getMessage(String key) {
        return messages.getMainMessage(key);
    }

    private String getSystemToAddresses() {
        return "balashov@tvoyapolka.info;aleksandrivko89@mail.ru"; //todo move to config
    }
}
