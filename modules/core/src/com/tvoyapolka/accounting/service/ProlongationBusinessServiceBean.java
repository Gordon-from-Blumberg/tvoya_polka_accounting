package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 16.03.19
 * <p>
 * $Id$
 */

import com.tvoyapolka.accounting.component.ProlongationBusinessComponent;
import com.tvoyapolka.accounting.entity.Prolongation;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Date;

@Service(ProlongationBusinessService.NAME)
public class ProlongationBusinessServiceBean implements ProlongationBusinessService {
    @Inject
    protected ProlongationBusinessComponent prolongationBusinessComponent;

    @Override
    public BigDecimal calculateCost(Prolongation prolongation) {
        return prolongationBusinessComponent.calculateCost(prolongation);
    }

    @Override
    public boolean isPromotionApplied(Prolongation prolongation) {
        return prolongationBusinessComponent.isPromotionApplied(prolongation);
    }

    @Override
    public Date calculateEndDate(Prolongation prolongation) {
        return prolongationBusinessComponent.calculateEndDate(prolongation);
    }
}