package com.tvoyapolka.accounting.service;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 11.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.DataManager;
import com.tvoyapolka.accounting.entity.Shop;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(ContractService.NAME)
public class ContractServiceBean implements ContractService {

    @Inject
    private DataManager dataManager;

    @Override
    public Integer getNextContractNumber(Shop shop) {
        Integer maxContractNumber = dataManager.loadValue(
                "select max(c.number) from tpa$Contract c where c.shop.id = :shop",
                Integer.class
        )
                .parameter("shop", shop)
                .one();

        if (maxContractNumber == null) {
            maxContractNumber = 0; //todo get value from config
        }

        return maxContractNumber + 1;
    }
}
