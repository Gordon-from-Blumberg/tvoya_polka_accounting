package com.tvoyapolka.accounting.listener;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 01.05.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.core.global.PersistenceHelper;
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener;
import com.tvoyapolka.accounting.entity.InvoiceProduct;
import com.tvoyapolka.accounting.entity.Product;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;
import com.tvoyapolka.accounting.utils.MapUtils;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

@Component("tpa_InvoiceProductEntityListener")
public class InvoiceProductEntityListener implements BeforeInsertEntityListener<InvoiceProduct> {

    private static final Map<InvoiceProduct.InvoiceProductType, Integer> INVOICE_TYPE_COEF = MapUtils
            .<InvoiceProduct.InvoiceProductType, Integer>start()
            .add(InvoiceProduct.InvoiceProductType.IN, 1)
            .add(InvoiceProduct.InvoiceProductType.OUT, -1)
            .build();

    @Inject
    protected DataManager dataManager;

    private Log log = LogFactory.getLog(getClass());

    @Override
    public void onBeforeInsert(InvoiceProduct entity, EntityManager entityManager) {
        final Product product = entity.getProduct();
        if (PersistenceHelper.isNew(product)) {
            log.debug("onBeforeInsert: product %s is new, set count %s from invoice",
                    product, entity.getCount());
            product.setCount(entity.getCount());
        } else {

            final List<InvoiceProduct> invoiceProducts = loadInvoiceProducts(product);
            int currentCount = 0;
            for (InvoiceProduct ip : invoiceProducts) {
                currentCount += INVOICE_TYPE_COEF.get(ip.getType()) * ip.getCount();
            }
            currentCount += INVOICE_TYPE_COEF.get(entity.getType()) * entity.getCount();

            log.debug("onBeforeInsert: product %s is already in base, load %s invoices and set count %s",
                    product, invoiceProducts.size(), currentCount);
            product.setCount(currentCount);
            entityManager.merge(product);
        }
    }

    private List<InvoiceProduct> loadInvoiceProducts(Product product) {
        return dataManager.loadList(
                LoadContext
                        .create(InvoiceProduct.class)
                        .setView("invoiceProduct.typeAndCount")
                        .setQuery(LoadContext.createQuery("select ip from tpa$InvoiceProduct ip " +
                                "where ip.product.id = :productId")
                                .setParameter("productId", product.getId()))
        );
    }
}