package com.tvoyapolka.accounting.listener;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 01.02.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.entity.User;
import com.tvoyapolka.accounting.component.DirectorGroupCreator;
import com.tvoyapolka.accounting.entity.Director;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component("tpa_DirectorEntityListener")
public class DirectorEntityListener implements BeforeInsertEntityListener<Director> {

    @Inject
    private DirectorGroupCreator directorGroupCreator;

    private Log log = LogFactory.getLog(getClass());

    @Override
    public void onBeforeInsert(Director entity, EntityManager entityManager) {
        Group directorGroup = entity.getGroup();
        if (directorGroup == null) {
            directorGroup = directorGroupCreator.createDirectorGroup(entity, entityManager);
            log.debug("onBeforeInsert: created group %s for director %s", directorGroup.getName(), entity);
            entity.setGroup(directorGroup);
        }

        final User user = entity.getTpUser().getUser();
        if (!directorGroup.equals(user.getGroup())) {
            user.setGroup(directorGroup);
            log.debug("onBeforeInsert: set group %s to user %s", directorGroup, user);
            entityManager.merge(user);
        }
    }
}
