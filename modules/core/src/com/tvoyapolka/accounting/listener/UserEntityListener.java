package com.tvoyapolka.accounting.listener;

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener;
import com.haulmont.cuba.core.listener.BeforeUpdateEntityListener;
import com.haulmont.cuba.security.entity.User;
import com.tvoyapolka.accounting.component.FullNameComposer;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;

import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component("tpa_UserEntityListener")
public class UserEntityListener
        implements BeforeInsertEntityListener<User>, BeforeUpdateEntityListener<User> {

    @Inject
    private FullNameComposer fullNameComposer;

    private Log log = LogFactory.getLog(getClass());

    @Override
    public void onBeforeInsert(User entity, EntityManager entityManager) {
        entity.setName( fullNameComposer.composeFullName(entity) );
        log.debug("onBeforeInsert: set name %s to user %s", entity.getName(), entity.getLogin());
    }

    @Override
    public void onBeforeUpdate(User entity, EntityManager entityManager) {
        entity.setName( fullNameComposer.composeFullName(entity) );
        log.debug("onBeforeUpdate: set name %s to user %s", entity.getName(), entity.getLogin());
    }
}