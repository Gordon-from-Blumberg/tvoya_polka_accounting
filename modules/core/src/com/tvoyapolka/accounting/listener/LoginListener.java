package com.tvoyapolka.accounting.listener;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 11.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.Events;
import com.haulmont.cuba.security.auth.events.UserLoggedInEvent;
import com.haulmont.cuba.security.global.UserSession;

import com.tvoyapolka.accounting.log.LogFactory;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
public class LoginListener {

    @Order(Events.LOWEST_PLATFORM_PRECEDENCE + 7)
    //@EventListener
    public void onUserLoggedIn(UserLoggedInEvent event) {
        final UserSession userSession = event.getUserSession();
    }
}
