package com.tvoyapolka.accounting.listener;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 01.02.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener;
import com.haulmont.cuba.security.entity.Group;
import com.tvoyapolka.accounting.component.DirectorGroupCreator;
import com.tvoyapolka.accounting.component.GroupCreator;
import com.tvoyapolka.accounting.entity.Address;
import com.tvoyapolka.accounting.entity.Director;
import com.tvoyapolka.accounting.entity.Shop;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;
import com.tvoyapolka.accounting.utils.DataUtils;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component("tpa_ShopEntityListener")
public class ShopEntityListener implements BeforeInsertEntityListener<Shop> {

    @Inject
    private DirectorGroupCreator directorGroupCreator;
    @Inject
    private GroupCreator groupCreator;

    private Log log = LogFactory.getLog(getClass());

    @Override
    public void onBeforeInsert(Shop entity, EntityManager entityManager) {
        entity.setDirector(adjustDirector(entity.getDirector(), entityManager));
        log.debug("onBeforeInsert: director = %s, director group = %s",
                entity.getDirector(), entity.getDirector().getGroup());

        final Group group = groupCreator.getBuilder()
                .parent( entity.getDirector().getGroup() )
                .name( composeName(entity) )
                .addConstraint("tpa$Shop", "{E}.director.id = :session$directorId")
                .addConstraint("tpa$Contragent", "{E}.shop.id = :session$shopId")
                .addConstraint("tpa$Contract", "{E}.shop.id = :session$shopId")
                .addSessionAttribute("shopId", "uuid", entity.getId().toString())
                .build();

        DataUtils.persistEntities(entityManager, group, group.getConstraints(), group.getSessionAttributes());
        log.debug("onBeforeInsert: created group %s for shop %s", group.getName(), entity);
        entity.setGroup(group);
    }

    private Director adjustDirector(Director director, EntityManager entityManager) {
        if (director.getGroup() == null) {
            director.setGroup(directorGroupCreator.createDirectorGroup(director, entityManager));
        }

        return entityManager.merge(director);
    }

    private String composeName(Shop shop) {
        final Address shopAddress = shop.getAddress();
        return String.format("%s, %s, %s",
                shopAddress.getTown(), shopAddress.getStreet(), shopAddress.getBuilding());
    }
}
