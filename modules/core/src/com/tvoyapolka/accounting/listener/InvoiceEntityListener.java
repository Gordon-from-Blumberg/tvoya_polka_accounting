package com.tvoyapolka.accounting.listener;

/**
 * Copyright (c) 2019 Tvoya Polka
 *
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 08.04.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.PersistenceHelper;
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener;
import com.tvoyapolka.accounting.entity.Invoice;
import com.tvoyapolka.accounting.entity.InvoiceProduct;
import com.tvoyapolka.accounting.entity.Product;
import com.tvoyapolka.accounting.entity.Shop;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component("tpa_InvoiceEntityListener")
public class InvoiceEntityListener implements BeforeInsertEntityListener<Invoice> {

    @Inject
    private DataManager dataManager;

    @Override
    public void onBeforeInsert(Invoice entity, EntityManager entityManager) {
        entity.setDateTime(new Date());

        List<Product> newProducts = entity.getProducts()
                .stream()
                .filter(ip -> ip.getType() == InvoiceProduct.InvoiceProductType.IN)
                .map(InvoiceProduct::getProduct)
                .filter(PersistenceHelper::isNew)
                .collect(Collectors.toList());

        if (!newProducts.isEmpty()) {
            Long lastArticul = getLastArticul(entity.getContract().getShop());

            for (Product newProduct : newProducts) {
                newProduct.setArticul( ++lastArticul );
            }
        }
    }

    private Long getLastArticul(Shop shop) {
        Long lastArticul = dataManager.loadValue(
                "select max(p.articul) from tpa$Product p where p.shop.id = :shopId",
                Long.class
        )
                .parameter("shopId", shop.getId())
                .one();

        return lastArticul != null ? lastArticul : shop.getArticulBase();
    }
}
