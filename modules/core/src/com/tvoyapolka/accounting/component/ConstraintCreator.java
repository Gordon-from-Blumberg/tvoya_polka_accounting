package com.tvoyapolka.accounting.component;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 13.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.security.entity.Constraint;
import com.haulmont.cuba.security.entity.Group;

import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.Set;

@Component
public class ConstraintCreator {

    @Inject
    private Metadata metadata;

    public Constraint create(Group group, String entity, String where) {
        final Constraint constraint = metadata.create(Constraint.class);
        constraint.setGroup(group);
        constraint.setEntityName(entity);
        constraint.setWhereClause(where);
        return constraint;
    }

    public Builder getBuilder(Group group) {
        return new Builder(group);
    }

    public class Builder {
        private final Group group;
        private final Set<Constraint> constraints = new HashSet<>();

        private Builder(Group group) {
            this.group = group;
        }

        public Builder add(String entity, String where) {
            constraints.add(create(group, entity, where));
            return this;
        }

        public Set<Constraint> build() {
            return constraints;
        }
    }
}
