package com.tvoyapolka.accounting.component;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 *
 * @author: Aleksandr Ivko
 * Created: 13.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.security.entity.Group;

import com.haulmont.cuba.security.entity.SessionAttribute;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashSet;

@Component
public class GroupCreator {

    @Inject
    private Metadata metadata;
    @Inject
    private ConstraintCreator constraintCreator;

    public Builder getBuilder() {
        return new Builder();
    }

    public class Builder {
        private final Group group = metadata.create(Group.class);
        private ConstraintCreator.Builder constraintBuilder;

        private Builder() {}

        public Builder parent(Group parent) {
            group.setParent(parent);
            return this;
        }

        public Builder name(String name) {
            group.setName(name);
            return this;
        }

        public Builder addConstraint(String entity, String where) {
            if (constraintBuilder == null) {
                constraintBuilder = constraintCreator.getBuilder(group);
            }
            constraintBuilder.add(entity, where);
            return this;
        }

        public Builder addSessionAttribute(String name, String datatype, String value) {
            if (group.getSessionAttributes() == null) {
                group.setSessionAttributes(new HashSet<>());
            }
            group.getSessionAttributes().add(createAttribute(name, datatype, value));
            return this;
        }

        public Group build() {
            group.setConstraints(constraintBuilder.build());
            return group;
        }

        private SessionAttribute createAttribute(String name, String datatype, String value) {
            final SessionAttribute attribute = metadata.create(SessionAttribute.class);
            attribute.setGroup(group);
            attribute.setName(name);
            attribute.setDatatype(datatype);
            attribute.setStringValue(value);
            return attribute;
        }
    }
}
