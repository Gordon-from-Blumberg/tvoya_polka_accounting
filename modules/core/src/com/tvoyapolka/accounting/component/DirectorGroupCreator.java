package com.tvoyapolka.accounting.component;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 18.02.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.security.entity.Group;
import com.tvoyapolka.accounting.entity.Director;
import com.tvoyapolka.accounting.service.GroupService;
import com.tvoyapolka.accounting.utils.DataUtils;
import org.springframework.stereotype.Component;

import javax.inject.Inject;


@Component
public class DirectorGroupCreator {

    @Inject
    private GroupService groupService;
    @Inject
    private GroupCreator groupCreator;

    public Group createDirectorGroup(Director director, EntityManager entityManager) {
        final Group group = groupCreator.getBuilder()
                .parent( groupService.getDefaultGroup() )
                .name( String.format("ИП %s", director.getName()) )
                .addConstraint("tpa$Contragent", "{E}.shop.director.id = :session$directorId")
                .addConstraint("tpa$Contract", "{E}.shop.director.id = :session$directorId")
                .addSessionAttribute("directorId", "uuid", director.getId().toString())
                .build();

        DataUtils.persistEntities(entityManager, group, group.getConstraints(), group.getSessionAttributes());

        return group;
    }
}