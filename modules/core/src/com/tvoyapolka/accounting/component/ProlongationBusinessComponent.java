package com.tvoyapolka.accounting.component;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 25.03.19
 * <p>
 * $Id$
 */

import com.tvoyapolka.accounting.entity.Prolongation;
import com.tvoyapolka.accounting.service.ShopInitService;
import com.tvoyapolka.accounting.utils.DateUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;

@Component
public class ProlongationBusinessComponent {
    private final static int PROMOTION_RENT_DURATION = 8;
    private final static BigDecimal PROMOTION_VALUE = new BigDecimal(50);

    public BigDecimal calculateCost(Prolongation prolongation) {
        if (prolongation == null
                || prolongation.getTariff() == null
                || prolongation.getDuration() == null) {
            return BigDecimal.ZERO;
        }

        BigDecimal tariffCost = prolongation.getTariff().getCost();

        if (isPromotionApplied(prolongation)) {
            tariffCost = tariffCost.subtract(PROMOTION_VALUE);
        }

        return new BigDecimal(prolongation.getDuration()).multiply(tariffCost);
    }

    public boolean isPromotionApplied(Prolongation prolongation) {
        return prolongation.getRentPlaceType() != null
                && prolongation.getDuration() != null
                && ShopInitService.SHELF.equalsIgnoreCase(prolongation.getRentPlaceType().getName())
                && prolongation.getDuration() >= PROMOTION_RENT_DURATION;
    }

    public Date calculateEndDate(Prolongation prolongation) {
        return prolongation.getStartDate() == null || prolongation.getDuration() == null
                ? null
                : DateUtils.addDays(DateUtils.addWeeks(prolongation.getStartDate(), prolongation.getDuration()), -1);
    }
}
