package com.tvoyapolka.accounting.component;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 01.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.sys.AppContext;
import com.haulmont.cuba.security.entity.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import static com.tvoyapolka.accounting.utils.Replacer.replace;

@Component //todo: check first, middle and last name for null
public class FullNameComposerBean implements FullNameComposer {

    @Override
    public String composeFullName(User user) {
        return replace(getPattern(), REGEX_SECTION, section -> replace(
                section,
                REGEX_PLACEHOLDER,
                placeholder -> NamePlaceholder.valueOf(placeholder).getName(user),
                true
        ));
    }

    private String getPattern() {
        return StringUtils.defaultIfBlank(
                AppContext.getProperty(FULL_NAME_PATTERN_PROPERTY_NAME),
                DEFAULT_PATTERN
        );
    }
}