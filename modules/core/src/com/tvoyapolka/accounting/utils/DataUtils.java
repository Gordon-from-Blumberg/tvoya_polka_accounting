package com.tvoyapolka.accounting.utils;

/**
 * Copyright (c) 2019 Tvoya Polka. All Rights Reserved.
 * <p>
 * Project: Tvoya Polka Accounting
 *
 * @author: Aleksandr Ivko
 * Created: 13.03.19
 * <p>
 * $Id$
 */

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.entity.Entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class DataUtils {
    private DataUtils() {}

    @SuppressWarnings("unchecked")
    public static void persistEntities(EntityManager entityManager, Object... objects) {
        final Set<Entity> enitiesToPersist = new HashSet<>();

        collectEntities(enitiesToPersist, objects);

        enitiesToPersist.forEach(entityManager::persist);
    }

    private static void collectEntities(Set<Entity> entities, Object... objects) {
        for (Object obj : objects) {
            if (obj instanceof Entity) {
                entities.add((Entity) obj);
            }

            if (obj instanceof Collection) {
                collectEntities(entities, ((Collection) obj).toArray());
            }
        }
    }
}
