package com.tvoyapolka.accounting.core;

import com.haulmont.cuba.core.global.Events;
import com.haulmont.cuba.core.sys.events.AppContextInitializedEvent;
import com.haulmont.cuba.core.sys.listener.EntityListenerManager;
import com.haulmont.cuba.security.entity.User;
import com.tvoyapolka.accounting.log.Log;
import com.tvoyapolka.accounting.log.LogFactory;
import com.tvoyapolka.accounting.properties.PropertiesHelper;
import com.tvoyapolka.accounting.service.TPEmailService;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component("tpa_AppLifecycle")
public class AppLifecycle {
    @Inject
    private EntityListenerManager entityListenerManager;
    @Inject
    private TPEmailService tpEmailService;

    private Log log = LogFactory.getLog(getClass());

    @SuppressWarnings("unused")
    @EventListener(AppContextInitializedEvent.class)
    @Order(Events.LOWEST_PLATFORM_PRECEDENCE + 100)
    public void initEntityListeners() {
        entityListenerManager.addListener(User.class, "tpa_UserEntityListener");

        if (!PropertiesHelper.isDevEnvironment() && PropertiesHelper.getBoolean("tpa.sendAppStartedEmail")) {
            log.debug("initEntityListeners: app started, send email");
            tpEmailService.sendAppStartedEmail();
        }
    }
}