-- begin SEC_USER
insert into SEC_USER (ID, CREATE_TS, UPDATE_TS, LOGIN, LOGIN_LC, NAME, ACTIVE)
values ('root', now(), now(), 'root', 'root', 'root', true)^
update SEC_USER
set GROUP_ID = 'e6a12ba2-3e10-42ee-ac29-8250eda9f73f'
where LOGIN = 'admin'^
-- end SEC_USER
-- begin SEC_GROUP
insert into SEC_GROUP (ID, CREATE_TS, VERSION, NAME, PARENT_ID)
values ('e6a12ba2-3e10-42ee-ac29-8250eda9f73f', now(), 0, 'Root', null)^
update SEC_GROUP 
set NAME = 'Твоя Полка',
 PARENT_ID = 'e6a12ba2-3e10-42ee-ac29-8250eda9f73f'
where ID = '0fa2b1a5-1d68-4d69-9fbd-dff348347f93'^
-- end SEC_GROUP
-- begin SEC_ROLE
insert into SEC_ROLE (ID, CREATE_TS, CREATED_BY, UPDATE_TS, UPDATED_BY, VERSION, NAME, LOC_NAME, ROLE_TYPE, IS_DEFAULT_ROLE, DESCRIPTION)
values ('1480a30a-b0bc-4622-b548-9c949a9cf75e', now(), 'root', now(), 'root', 0, 'Default', 'По умолчанию', 30, true, 'Запрещает все действия, ставится по умолчанию всем новым пользователям.[hide]'),
       ('e4cd5140-9434-4533-83c2-425b084e2a1f', now(), 'root', now(), 'root', 1, 'Seller', 0, false, 'Продавец, администратор.'),
       ('a11c859f-7d8b-4504-89ab-1d1bb7501c0d', now(), 'root', now(), 'root', 1, 'Director', 0, false, 'Директор магазина.')^
-- end SEC_ROLE
-- begin SEC_PERMISSION
insert into SEC_PERMISSION (ID, CREATE_TS, CREATED_BY, VERSION, UPDATE_TS, UPDATED_BY, PERMISSION_TYPE, TARGET, VALUE_, ROLE_ID)
values (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'mainWindow', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 0, now(), 'root', 20, 'sec$Role:create', 0, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 0, now(), 'root', 20, 'sec$Role:delete', 0, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 0, now(), 'root', 20, 'sec$Role:update', 0, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 0, now(), 'root', 20, 'sec$Permission:create', 0, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 0, now(), 'root', 20, 'sec$Permission:delete', 0, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 0, now(), 'root', 20, 'sec$Permission:update', 0, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'sec$User.changePassword', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'sec$User.read', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'sec$User.update', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'settings', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'aboutWindow', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Address:read', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Address:create', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Address:update', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Address:delete', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Passport:read', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Passport:create', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Passport:update', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Passport:delete', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$Passport.edit', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),

       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$Contract.create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Contract:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Contract:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Contract:update', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Shop:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Director:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$TPUser:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$TPUser:update', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'sec$User:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'sec$User:update', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Prolongation:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Prolongation:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Prolongation:update', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$RentPlaceType:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Tariff:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Contragent:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Contragent:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Contragent:update', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Shelf:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Shelf:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Shelf:update', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$Prolongation.create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$Shelf.edit', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$Shelf.multipleCreate', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Product:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Product:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Product:update', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Discount:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Discount:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Discount:update', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$ContractTransaction:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$ContractTransaction:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'contractPayment', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Invoice:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Invoice:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$InvoiceProduct:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$InvoiceProduct:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$Invoice.create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$InvoiceProduct.create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'existing-product-list', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'productListFrame', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),

       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$Shop.admin.browse', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$Director.admin.edit', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$TPUser.admin.browse', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$TPUser.admin.edit', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$Shop.admin.edit', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa$RentPlaceType.edit', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 10, 'tpa.administration', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Shop:update', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Director:update', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$TPUser:update', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$TPUser:create', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'sec$User:create', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$RentPlaceType:create', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$RentPlaceType:update', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$RentPlaceType:delete', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'sec$Role:read', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Discount:delete', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d')^
-- end SEC_PERMISSION
-- begin TPA_TP_USER
insert into TPA_TP_USER (ID, CREATE_TS, CREATED_BY, UPDATE_TS, UPDATED_BY, VERSION, USER_ID, PHONE)
values ('e007d0b3-2b2d-48b1-899a-592be21036ee', now(), 'root', now(), 'root', 0, '60885987-1b61-4247-94c7-dff348347f93', '123')^
-- end TPA_TP_USER    
-- begin TPA_TARIFF
insert into TPA_TARIFF (ID, CREATE_TS, CREATED_BY, UPDATE_TS, UPDATED_BY, VERSION, COST)
values ('f74654d8-f378-4f07-b6c0-f15cedd02e6f', now(), 'root', now(), 'root', 0, 50),
  ('b57021bf-7841-4bc1-9983-a0d268af57c1', now(), 'root', now(), 'root', 0, 100),
  ('ab24d0ae-53e3-4720-8566-4d7a2a21cd27', now(), 'root', now(), 'root', 0, 150),
  ('4ceebb9d-fff3-44f9-8840-91bf1d769df9', now(), 'root', now(), 'root', 0, 200),
  ('6fc2899a-f1b1-40ed-bb59-1b17c21913f3', now(), 'root', now(), 'root', 0, 250),
  ('f9c1362b-1ad4-4c29-a0e6-cdf18384c250', now(), 'root', now(), 'root', 0, 300),
  ('8bf3301d-aedb-4d0d-b451-75fb477f5f19', now(), 'root', now(), 'root', 0, 350),
  ('1cf5583b-c0ae-4562-a715-7c6f75164355', now(), 'root', now(), 'root', 0, 400),
  ('40f6d731-ab74-4f76-b608-d2b1964d61aa', now(), 'root', now(), 'root', 0, 450),
  ('1d0158fb-e865-4165-816f-0ad44550db17', now(), 'root', now(), 'root', 0, 500)^
-- end TPA_TARIFF
