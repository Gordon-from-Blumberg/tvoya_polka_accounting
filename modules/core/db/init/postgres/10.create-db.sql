-- begin TPA_CONTRAGENT
create table TPA_CONTRAGENT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    ACTIVE boolean not null default true,
    --
    ADDRESS_POSTCODE varchar(16),
    ADDRESS_TOWN varchar(64),
    ADDRESS_STREET varchar(64),
    ADDRESS_BUILDING varchar(8),
    ADDRESS_SUBBUILDING varchar(8),
    ADDRESS_FLAT varchar(8),
    --
    NAME varchar(255) not null,
    SECOND_NAME varchar(32),
    LAST_NAME varchar(32),
    SHOP_ID uuid,
    ACTIVE_CONTRACT_ID uuid,
    PHONE varchar(20),
    TYPE_ integer not null,
    PASSPORT_ID uuid,
    INN varchar(20),
    OGRN_IP varchar(20),
    OGRN varchar(20),
    KPP varchar(20),
    COMMENT varchar(255),
    --
    LEGAL_ADDRESS_POSTCODE varchar(16),
    LEGAL_ADDRESS_TOWN varchar(64),
    LEGAL_ADDRESS_STREET varchar(64),
    LEGAL_ADDRESS_BUILDING varchar(8),
    LEGAL_ADDRESS_SUBBUILDING varchar(8),
    LEGAL_ADDRESS_FLAT varchar(8),
    --
    primary key (ID)
)^
-- end TPA_CONTRAGENT
-- begin TPA_TP_USER
create table TPA_TP_USER (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    ADDRESS_POSTCODE varchar(16) not null,
    ADDRESS_TOWN varchar(64) not null,
    ADDRESS_STREET varchar(64) not null,
    ADDRESS_BUILDING varchar(8) not null,
    ADDRESS_SUBBUILDING varchar(8),
    ADDRESS_FLAT varchar(8),
    --
    USER_ID uuid not null,
    PHONE varchar(20) not null,
    INN varchar(20),
    --
    primary key (ID)
)^
-- end TPA_TP_USER
-- begin TPA_SHOP
create table TPA_SHOP (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    ADDRESS_POSTCODE varchar(16),
    ADDRESS_TOWN varchar(64),
    ADDRESS_STREET varchar(64),
    ADDRESS_BUILDING varchar(8),
    ADDRESS_SUBBUILDING varchar(8),
    ADDRESS_FLAT varchar(8),
    --
    WORK_PHONE varchar(20) not null,
    DIRECTOR_ID uuid,
    GROUP_ID uuid,
    ARTICUL_BASE bigint not null,
    --
    primary key (ID)
)^
-- end TPA_SHOP
-- begin TPA_PASSPORT
create table TPA_PASSPORT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    REGISTRY_ADDRESS_POSTCODE varchar(16),
    REGISTRY_ADDRESS_TOWN varchar(64),
    REGISTRY_ADDRESS_STREET varchar(64),
    REGISTRY_ADDRESS_BUILDING varchar(8),
    REGISTRY_ADDRESS_SUBBUILDING varchar(8),
    REGISTRY_ADDRESS_FLAT varchar(8),
    --
    FIRST_NAME varchar(255) not null,
    SECOND_NAME varchar(255),
    LAST_NAME varchar(255) not null,
    SERIES integer not null,
    NUMBER_ integer not null,
    DATE_OF_ISSUE date not null,
    DEPARTMENT_CODE varchar(10) not null,
    --
    primary key (ID)
)^
-- end TPA_PASSPORT
-- begin TPA_TARIFF
create table TPA_TARIFF (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    COST decimal(19, 2) not null,
    --
    primary key (ID)
)^
-- end TPA_TARIFF
-- begin TPA_RENT_PLACE_TYPE
create table TPA_RENT_PLACE_TYPE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    SHOP_ID uuid not null,
    NAME varchar(64) not null,
    --
    primary key (ID)
)^
-- end TPA_RENT_PLACE_TYPE
-- begin TPA_RENT_PLACE_TYPE_TARIFF_LINK
create table TPA_RENT_PLACE_TYPE_TARIFF_LINK (
    RENT_PLACE_TYPE_ID uuid,
    TARIFF_ID uuid,
    primary key (RENT_PLACE_TYPE_ID, TARIFF_ID)
)^
-- end TPA_RENT_PLACE_TYPE_TARIFF_LINK
-- begin TPA_DIRECTOR
create table TPA_DIRECTOR (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TP_USER_ID uuid,
    GROUP_ID uuid,
    OGRN_IP varchar(20),
    --
    LEGAL_ADDRESS_POSTCODE varchar(16),
    LEGAL_ADDRESS_TOWN varchar(64),
    LEGAL_ADDRESS_STREET varchar(64),
    LEGAL_ADDRESS_BUILDING varchar(8),
    LEGAL_ADDRESS_SUBBUILDING varchar(8),
    LEGAL_ADDRESS_FLAT varchar(8),
    --
    primary key (ID)
)^
-- end TPA_DIRECTOR
-- begin TPA_DOCUMENT
create table TPA_DOCUMENT (
    CODE varchar(32),
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(32) not null,
    TEXT text not null,
    --
    primary key (CODE)
)^
-- end TPA_DOCUMENT
-- begin TPA_CONTRACT
create table TPA_CONTRACT (
  ID uuid,
  VERSION integer not null,
  CREATE_TS timestamp,
  CREATED_BY varchar(50),
  UPDATE_TS timestamp,
  UPDATED_BY varchar(50),
  DELETE_TS timestamp,
  DELETED_BY varchar(50),
  --
  SHOP_ID uuid,
  CONTRAGENT_ID uuid,
  NUMBER_ integer not null,
  DATE_ date not null,
  END_DATE date,
  CLOSED boolean,
  DEAL_TYPE integer not null,
  PERCENT decimal(19, 2),
  --
  primary key (ID)
)^
-- end TPA_CONTRACT
-- begin TPA_SHELF
create table TPA_SHELF (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    SHOP_ID uuid,
    NUMBER_ integer not null,
    STATE integer not null default 10,
    ACTIVE_CONTRACT_ID uuid,
    TARIFF_ID uuid,
    --
    primary key (ID)
)^
-- end TPA_SHELF
-- begin TPA_PROLONGATION
create table TPA_PROLONGATION (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CONTRACT_ID uuid,
    SHELF_ID uuid,
    RENT_PLACE_TYPE_ID uuid,
    TARIFF_ID uuid,
    DURATION integer,
    DATE_ date not null,
    START_DATE date,
    END_DATE date,
    COST decimal(19, 2),
    PAID decimal(19, 2),
    NUMBER_ integer not null,
    --
    primary key (ID)
)^
-- end TPA_PROLONGATION
-- begin TPA_PRODUCT
create table TPA_PRODUCT (
  ID uuid,
  VERSION integer not null,
  CREATE_TS timestamp,
  CREATED_BY varchar(50),
  UPDATE_TS timestamp,
  UPDATED_BY varchar(50),
  DELETE_TS timestamp,
  DELETED_BY varchar(50),
  --
  NAME varchar(100) not null,
  DESCRIPTION varchar(256),
  SHOP_ID uuid,
  CONTRACT_ID uuid,
  CONTRAGENT_ID uuid,
  COUNT integer not null,
  COST decimal(19, 2) not null,
  DISCOUNT_ID uuid,
  RENT_PLACE_TYPE_ID uuid,
  SHELF_ID uuid,
  MARKUP decimal(19, 2),
  BASE_COST decimal(19, 2),
  --
  primary key (ID)
)^
--  end TPA_PRODUCT
--  begin TPA_DISCOUNT
create table TPA_DISCOUNT (
  ID uuid,
  VERSION integer not null,
  CREATE_TS timestamp,
  CREATED_BY varchar(50),
  UPDATE_TS timestamp,
  UPDATED_BY varchar(50),
  DELETE_TS timestamp,
  DELETED_BY varchar(50),
  --
  SHOP_ID uuid,
  CONTRACT_ID uuid,
  VALUE integer not null,
  END_DATE date,
  --
  primary key (ID)
)^
--  end TPA_DISCOUNT
--  begin TPA_CONTRACT_TRANSACTION
create table TPA_CONTRACT_TRANSACTION (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DIRECTION integer not null,
    PAYMENT_TYPE integer not null,
    DATE timestamp not null,
    VALUE decimal(19, 2) not null,
    CONTRACT_ID uuid,
    --
    primary key (ID)
)^
--  end TPA_CONTRACT_TRANSACTION
--  begin TPA_INVOICE
create table TPA_INVOICE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CONTRACT_ID uuid,
    DATE_TIME timestamp not null,
    TYPE integer not null,
    TOTAL_COST decimal(19, 2),
    ORDER_COST decimal(19, 2),
    --
    primary key (ID)
)^
--  end TPA_INVOICE
--  begin TPA_INVOICE_PRODUCT
create table TPA_INVOICE_PRODUCT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    INVOICE_ID uuid,
    PRODUCT_ID uuid,
    COUNT integer not null,
    PREV_COST decimal(19, 2),
    COST decimal(19, 2),
    TYPE integer not null,
    --
    primary key (ID)
)^
--  end TPA_INVOICE_PRODUCT
