insert into SEC_PERMISSION (ID, CREATE_TS, CREATED_BY, VERSION, UPDATE_TS, UPDATED_BY, PERMISSION_TYPE, TARGET, VALUE_, ROLE_ID)
values ('b48500b8-0b2a-415e-9bab-cd9e96cefab9', now(), 'root', 1, now(), 'root', 10, 'tpa$TPUser.admin.browse', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       ('6e9711ad-4764-47ef-9c30-b05df2763a83', now(), 'root', 1, now(), 'root', 10, 'tpa$TPUser.admin.edit', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       ('e5501f6a-438f-42d7-aaf8-59357a60a428', now(), 'root', 1, now(), 'root', 10, 'tpa$Shop.admin.edit', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       ('e98fdbae-ac99-4cad-8ace-f7d7e7ee4488', now(), 'root', 1, now(), 'root', 10, 'tpa$RentPlaceType.edit', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),

       ('0328d44a-1d69-484d-83eb-3d2acf747ab8', now(), 'root', 1, now(), 'root', 20, 'tpa$Shop:update', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       ('c78537de-7cc1-4d4b-a264-0e7d4ab3fd15', now(), 'root', 1, now(), 'root', 20, 'tpa$Director:update', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       ('04495492-234e-48f7-afaa-dd61cee7f9a0', now(), 'root', 1, now(), 'root', 20, 'tpa$TPUser:update', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       ('15fec84a-0879-49c1-8fd0-589d2693c029', now(), 'root', 1, now(), 'root', 20, 'tpa$RentPlaceType:create', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       ('a091a095-f9c9-40ed-87de-faaf515106b1', now(), 'root', 1, now(), 'root', 20, 'tpa$RentPlaceType:update', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       ('7332d047-10ac-460a-87bb-462a26d0472b', now(), 'root', 1, now(), 'root', 20, 'tpa$RentPlaceType:delete', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d'),
       ('a74553a7-9779-4d67-b2b3-b46327c43445', now(), 'root', 1, now(), 'root', 20, 'sec$Role:read', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d');