create table TPA_PRODUCT (
  ID uuid,
  VERSION integer not null,
  CREATE_TS timestamp,
  CREATED_BY varchar(50),
  UPDATE_TS timestamp,
  UPDATED_BY varchar(50),
  DELETE_TS timestamp,
  DELETED_BY varchar(50),
  --
  NAME varchar(100) not null,
  DESCRIPTION varchar(256),
  SHOP_ID uuid,
  CONTRACT_ID uuid,
  CONTRAGENT_ID uuid,
  COUNT integer not null,
  COST decimal(19, 2) not null,
  DISCOUNT_ID uuid,
  RENT_PLACE_TYPE_ID uuid,
  SHELF_ID uuid,
  --
  primary key (ID)
);

alter table TPA_PRODUCT add constraint FK_TPA_PRODUCT_ON_SHOP foreign key (SHOP_ID) references TPA_SHOP(ID);
alter table TPA_PRODUCT add constraint FK_TPA_PRODUCT_ON_CONTRACT foreign key (CONTRACT_ID) references TPA_CONTRACT(ID);
alter table TPA_PRODUCT add constraint FK_TPA_PRODUCT_ON_CONTRAGENT foreign key (CONTRAGENT_ID) references TPA_CONTRAGENT(ID);
alter table TPA_PRODUCT add constraint FK_TPA_PRODUCT_ON_DISCOUNT foreign key (DISCOUNT_ID) references TPA_DISCOUNT(ID);
alter table TPA_PRODUCT add constraint FK_TPA_PRODUCT_ON_RENT_PLACE_TYPE foreign key (RENT_PLACE_TYPE_ID) references TPA_RENT_PLACE_TYPE(ID);
alter table TPA_PRODUCT add constraint FK_TPA_PRODUCT_ON_SHELF_ID foreign key (SHELF_ID) references TPA_SHELF(ID);
create index IDX_TPA_PRODUCT_ON_SHOP on TPA_PRODUCT (SHOP_ID);
create index IDX_TPA_PRODUCT_ON_CONTRACT on TPA_PRODUCT (CONTRACT_ID);
create index IDX_TPA_PRODUCT_ON_CONTRAGENT on TPA_PRODUCT (CONTRAGENT_ID);
create index IDX_TPA_PRODUCT_ON_RENT_PLACE_TYPE on TPA_PRODUCT (RENT_PLACE_TYPE_ID);
create index IDX_TPA_PRODUCT_ON_SHELF on TPA_PRODUCT (SHELF_ID);
