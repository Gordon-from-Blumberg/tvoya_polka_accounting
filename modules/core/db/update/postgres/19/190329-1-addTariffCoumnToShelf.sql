alter table TPA_SHELF add column TARIFF_ID uuid;

alter table TPA_SHELF add constraint FK_TPA_SHELF_ON_TARIFF foreign key (TARIFF_ID) references TPA_TARIFF;
