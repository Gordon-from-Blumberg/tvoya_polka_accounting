alter table TPA_INVOICE add column TOTAL_COST decimal(19, 2);

create table _TEMP (
  ID uuid,
  TOTAL_COST decimal(19, 2)
);

insert into _TEMP (ID, TOTAL_COST) (
  select i.ID, sum(ip.COUNT * p.COST) as TOTAL_COST from TPA_INVOICE i
    join TPA_INVOICE_PRODUCT ip on ip.INVOICE_ID = i.ID
    join TPA_PRODUCT p on p.ID = ip.PRODUCT_ID
    group by i.ID
);

update TPA_INVOICE i set TOTAL_COST = t.TOTAL_COST
from _TEMP t where t.ID = i.ID;

alter table TPA_INVOICE alter column TOTAL_COST set not null;

drop table _TEMP;
