alter table TPA_TP_USER add constraint FK_TPA_TP_USER_ON_USER foreign key (USER_ID) references SEC_USER(ID);
alter table TPA_TP_USER add constraint FK_TPA_TP_USER_ON_ADDRESS foreign key (ADDRESS_ID) references TPA_ADDRESS(ID);
create unique index IDX_TPA_TP_USER_UK_USER_ID on TPA_TP_USER (USER_ID) where DELETE_TS is null ;
create index IDX_TPA_TP_USER_ON_USER on TPA_TP_USER (USER_ID);
create index IDX_TPA_TP_USER_ON_ADDRESS on TPA_TP_USER (ADDRESS_ID);
