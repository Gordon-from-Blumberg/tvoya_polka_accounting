create table TPA_INVOICE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CONTRACT_ID uuid,
    DATE_TIME timestamp not null,
    TYPE integer not null,
    --
    primary key (ID)
);

create table TPA_INVOICE_PRODUCT (
  ID uuid,
  VERSION integer not null,
  CREATE_TS timestamp,
  CREATED_BY varchar(50),
  UPDATE_TS timestamp,
  UPDATED_BY varchar(50),
  DELETE_TS timestamp,
  DELETED_BY varchar(50),
  --
  INVOICE_ID uuid,
  PRODUCT_ID uuid,
  COUNT integer not null,
  --
  primary key (ID)
);

alter table TPA_INVOICE add constraint FK_TPA_INVOICE_ON_CONTRACT foreign key (CONTRACT_ID) references TPA_CONTRACT(ID);
create index IDX_TPA_INVOICE_ON_CONTRACT on TPA_INVOICE (CONTRACT_ID);

alter table TPA_INVOICE_PRODUCT add constraint FK_TPA_INVOICE_PRODUCT_ON_INVOICE foreign key (INVOICE_ID) references TPA_INVOICE(ID);
alter table TPA_INVOICE_PRODUCT add constraint FK_TPA_INVOICE_PRODUCT_ON_PRODUCT foreign key (PRODUCT_ID) references TPA_PRODUCT(ID);
create index IDX_TPA_INVOICE_PRODUCT_ON_INVOICE on TPA_INVOICE_PRODUCT (INVOICE_ID);
create index IDX_TPA_INVOICE_PRODUCT_ON_PRODUCT on TPA_INVOICE_PRODUCT (PRODUCT_ID);
