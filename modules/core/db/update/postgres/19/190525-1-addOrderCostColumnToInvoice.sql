alter table TPA_INVOICE alter column TOTAL_COST drop not null ;
alter table TPA_INVOICE add column ORDER_COST decimal(19, 2);
