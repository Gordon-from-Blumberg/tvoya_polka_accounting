alter table TPA_INVOICE_PRODUCT
    add column PREV_COST decimal(19, 2),
    add column COST decimal(19, 2);
