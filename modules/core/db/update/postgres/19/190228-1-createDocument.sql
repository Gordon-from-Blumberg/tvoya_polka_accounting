create table TPA_DOCUMENT (
    CODE varchar(32),
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(32) not null,
    TEXT text not null,
    --
    primary key (CODE)
);