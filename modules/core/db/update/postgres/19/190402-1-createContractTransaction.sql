create table TPA_CONTRACT_TRANSACTION (
  ID uuid,
  VERSION integer not null,
  CREATE_TS timestamp,
  CREATED_BY varchar(50),
  UPDATE_TS timestamp,
  UPDATED_BY varchar(50),
  DELETE_TS timestamp,
  DELETED_BY varchar(50),
  --
  DIRECTION integer not null,
  PAYMENT_TYPE integer not null,
  DATE timestamp not null,
  VALUE decimal(19, 2) not null,
  CONTRACT_ID uuid,
  --
  primary key (ID)
);

alter table TPA_CONTRACT_TRANSACTION add constraint FK_TPA_CONTRACT_TRANSACTION_ON_CONTRACT foreign key (CONTRACT_ID) references TPA_CONTRACT(ID);
create index IDX_TPA_CONTRACT_TRANSACTION_ON_CONTRACT on TPA_CONTRACT_TRANSACTION (CONTRACT_ID);
