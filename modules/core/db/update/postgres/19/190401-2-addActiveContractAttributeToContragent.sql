alter table TPA_CONTRAGENT add column ACTIVE_CONTRACT_ID uuid;

alter table TPA_CONTRAGENT add constraint FK_TPA_CONTRAGENT_ON_CONTRACT foreign key (ACTIVE_CONTRACT_ID) references TPA_CONTRACT(ID);
