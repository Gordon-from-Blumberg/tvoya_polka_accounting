insert into SEC_PERMISSION (ID, CREATE_TS, CREATED_BY, VERSION, UPDATE_TS, UPDATED_BY, PERMISSION_TYPE, TARGET, VALUE_, ROLE_ID)
values (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Product:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Product:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Product:update', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f');