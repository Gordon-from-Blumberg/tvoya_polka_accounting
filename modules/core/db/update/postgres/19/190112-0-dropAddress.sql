alter table tpa_address rename to TPA_ADDRESS__U08063 ;
alter table tpa_contragent drop constraint FK_TPA_CONTRAGENT_ON_ADDRESS ;
alter table tpa_passport drop constraint FK_TPA_PASSPORT_ON_REGISTRY_ADDRESS ;
alter table tpa_shop drop constraint FK_TPA_SHOP_ON_ADDRESS ;
alter table tpa_tp_user drop constraint FK_TPA_TP_USER_ON_ADDRESS ;
