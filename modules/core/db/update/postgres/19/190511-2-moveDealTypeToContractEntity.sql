alter table TPA_CONTRACT add column DEAL_TYPE integer;

update TPA_CONTRACT cnct
set DEAL_TYPE = cagnt.DEAL_TYPE
from TPA_CONTRAGENT cagnt
where cnct.CONTRAGENT_ID = cagnt.ID ;

alter table TPA_CONTRACT alter column DEAL_TYPE set not null ;

alter table TPA_CONTRAGENT drop column DEAL_TYPE;
