alter table TPA_PROLONGATION add column TARIFF_ID uuid;

alter table TPA_PROLONGATION add constraint FK_TPA_PROLONGATION_ON_TARIFF foreign key (TARIFF_ID) references TPA_TARIFF;
