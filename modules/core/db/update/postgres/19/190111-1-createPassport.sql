create table TPA_PASSPORT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIRST_NAME varchar(255) not null,
    SECOND_NAME varchar(255),
    LAST_NAME varchar(255) not null,
    SERIES integer not null,
    NUMBER_ integer not null,
    DATE_OF_ISSUE date not null,
    DEPARTMENT_CODE varchar(10) not null,
    REGISTRY_ADDRESS_ID uuid not null,
    --
    primary key (ID)
);
