insert into SEC_PERMISSION (ID, CREATE_TS, CREATED_BY, VERSION, UPDATE_TS, UPDATED_BY, PERMISSION_TYPE, TARGET, VALUE_, ROLE_ID)
values (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Discount:create', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Discount:read', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),
       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Discount:update', 1, 'e4cd5140-9434-4533-83c2-425b084e2a1f'),

       (uuid_generate_v4(), now(), 'root', 1, now(), 'root', 20, 'tpa$Discount:delete', 1, 'a11c859f-7d8b-4504-89ab-1d1bb7501c0d');