alter table TPA_SHOP add constraint FK_TPA_SHOP_ON_ADDRESS foreign key (ADDRESS_ID) references TPA_ADDRESS(ID);
create index IDX_TPA_SHOP_ON_ADDRESS on TPA_SHOP (ADDRESS_ID);
