insert into SEC_PERMISSION (ID, CREATE_TS, CREATED_BY, VERSION, UPDATE_TS, UPDATED_BY, PERMISSION_TYPE, TARGET, VALUE_, ROLE_ID)
values ('b080df9d-a374-4b51-94e7-75a6fc273ca4', now(), 'root', 1, now(), 'root', 10, 'sec$User.changePassword', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
      ('c6a1c6a2-cc5a-44eb-b737-93c9d2311b8e', now(), 'root', 1, now(), 'root', 20, 'sec$User.read', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e'),
      ('7a1b7a86-e817-47a4-a54a-f70e1e7bb460', now(), 'root', 1, now(), 'root', 20, 'sec$User.update', 1, '1480a30a-b0bc-4622-b548-9c949a9cf75e');