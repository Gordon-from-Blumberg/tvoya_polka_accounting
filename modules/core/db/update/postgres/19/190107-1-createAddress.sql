create table TPA_ADDRESS (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    POSTCODE varchar(20) not null,
    TOWN varchar(100) not null,
    STREET varchar(100) not null,
    BUILDING varchar(10) not null,
    SUBBUILDING varchar(10),
    FLAT integer,
    --
    primary key (ID)
);
