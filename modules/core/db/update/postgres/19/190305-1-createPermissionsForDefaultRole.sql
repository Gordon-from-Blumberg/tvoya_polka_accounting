DO $$
declare def_role_id uuid;
begin
  def_role_id := '1480a30a-b0bc-4622-b548-9c949a9cf75e';

  insert into SEC_PERMISSION (ID, CREATE_TS, CREATED_BY, VERSION, UPDATE_TS, UPDATED_BY, PERMISSION_TYPE, TARGET, VALUE_, ROLE_ID)
  values ('d5be8628-eaf2-cb2c-5336-ffbe54e56661', now(), 'root', 0, now(), 'root', 20, 'sec$Role:create', 0, def_role_id),
         ('42c1fb96-b3e3-81fd-03be-e0f9c3443523', now(), 'root', 0, now(), 'root', 20, 'sec$Role:delete', 0, def_role_id),
         ('ff51334a-3f04-2660-3895-b3887d96b474', now(), 'root', 0, now(), 'root', 20, 'sec$Role:update', 0, def_role_id),
         ('a493da98-3e79-5a78-47c1-64cbbeca84c7', now(), 'root', 0, now(), 'root', 20, 'sec$Permission:create', 0, def_role_id),
         ('fb50c93a-aa62-073b-a14f-2522888055c1', now(), 'root', 0, now(), 'root', 20, 'sec$Permission:delete', 0, def_role_id),
         ('5782efcc-ced8-e612-9612-97b9fd07b33b', now(), 'root', 0, now(), 'root', 20, 'sec$Permission:update', 0, def_role_id);

  update SEC_ROLE
  set UPDATE_TS = now(),
      UPDATED_BY = 'root',
      LOC_NAME = 'По умолчанию',
      DESCRIPTION = 'Запрещает все действия, ставится по умолчанию всем новым пользователям.[hide]'
   where ID = def_role_id;
END $$;