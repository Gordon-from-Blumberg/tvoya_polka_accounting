alter table TPA_DIRECTOR add constraint FK_TPA_DIRECTOR_TP_USER foreign key (TP_USER_ID) references TPA_TP_USER(ID);
alter table TPA_DIRECTOR add constraint FK_TPA_DIRECTOR_GROUP foreign key (GROUP_ID) references SEC_GROUP(ID);
create unique index IDX_TPA_DIRECTOR_TP_USER_ID_UNQ on TPA_DIRECTOR (TP_USER_ID) where DELETE_TS is null ;
create unique index IDX_TPA_DIRECTOR_GROUP_ID_UNQ on TPA_DIRECTOR (GROUP_ID) where DELETE_TS is null ;
create index IDX_DIRECTOR_TP_USER_ID on TPA_DIRECTOR (TP_USER_ID);
create index IDX_DIRECTOR_GROUP_ID on TPA_DIRECTOR (GROUP_ID);
