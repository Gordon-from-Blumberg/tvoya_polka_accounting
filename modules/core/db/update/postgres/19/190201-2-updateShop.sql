alter table TPA_SHOP drop constraint FK_TPA_SHOP_ON_DIRECTOR;
alter table TPA_SHOP add constraint FK_TPA_SHOP_ON_DIRECTOR foreign key (DIRECTOR_ID) references TPA_DIRECTOR(ID);
alter table TPA_SHOP add column GROUP_ID uuid;
update TPA_SHOP set GROUP_ID = '0fa2b1a5-1d68-4d69-9fbd-dff348347f93';
alter table TPA_SHOP alter column GROUP_ID set not null ;
