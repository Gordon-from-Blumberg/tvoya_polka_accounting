create table TPA_DIRECTOR (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TP_USER_ID uuid not null,
    GROUP_ID uuid not null,
    OGRN_IP varchar(20),
    --
    LEGAL_ADDRESS_POSTCODE varchar(16),
    LEGAL_ADDRESS_TOWN varchar(64),
    LEGAL_ADDRESS_STREET varchar(64),
    LEGAL_ADDRESS_BUILDING varchar(8),
    LEGAL_ADDRESS_SUBBUILDING varchar(8),
    LEGAL_ADDRESS_FLAT varchar(8),
    --
    primary key (ID)
);
