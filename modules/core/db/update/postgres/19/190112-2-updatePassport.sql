alter table TPA_PASSPORT rename column registry_address_id to registry_address_id__u75525 ;
alter table TPA_PASSPORT alter column registry_address_id__u75525 drop not null ;
drop index IDX_TPA_PASSPORT_ON_REGISTRY_ADDRESS ;
alter table TPA_PASSPORT add column REGISTRY_ADDRESS_POSTCODE varchar(16) ^
update TPA_PASSPORT set REGISTRY_ADDRESS_POSTCODE = '' where REGISTRY_ADDRESS_POSTCODE is null ;
alter table TPA_PASSPORT alter column REGISTRY_ADDRESS_POSTCODE set not null ;
alter table TPA_PASSPORT add column REGISTRY_ADDRESS_TOWN varchar(64) ^
update TPA_PASSPORT set REGISTRY_ADDRESS_TOWN = '' where REGISTRY_ADDRESS_TOWN is null ;
alter table TPA_PASSPORT alter column REGISTRY_ADDRESS_TOWN set not null ;
alter table TPA_PASSPORT add column REGISTRY_ADDRESS_STREET varchar(64) ^
update TPA_PASSPORT set REGISTRY_ADDRESS_STREET = '' where REGISTRY_ADDRESS_STREET is null ;
alter table TPA_PASSPORT alter column REGISTRY_ADDRESS_STREET set not null ;
alter table TPA_PASSPORT add column REGISTRY_ADDRESS_BUILDING varchar(8) ^
update TPA_PASSPORT set REGISTRY_ADDRESS_BUILDING = '' where REGISTRY_ADDRESS_BUILDING is null ;
alter table TPA_PASSPORT alter column REGISTRY_ADDRESS_BUILDING set not null ;
alter table TPA_PASSPORT add column REGISTRY_ADDRESS_SUBBUILDING varchar(8) ;
alter table TPA_PASSPORT add column REGISTRY_ADDRESS_FLAT varchar(8) ;
