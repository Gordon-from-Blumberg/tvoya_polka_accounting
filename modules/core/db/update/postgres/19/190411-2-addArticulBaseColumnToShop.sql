alter table TPA_SHOP add column ARTICUL_BASE bigint;

update TPA_SHOP sh
set ARTICUL_BASE = sub.idx * 1000000000
from (select ID, row_number() over (order by CREATE_TS) as idx from TPA_SHOP) sub
where sh.ID = sub.ID;

alter table TPA_SHOP alter column ARTICUL_BASE set not null ;

create unique index IDX_TPA_SHOP_ON_ARTICUL_BASE_UNQ on TPA_SHOP (ARTICUL_BASE) where DELETE_TS is null ;
