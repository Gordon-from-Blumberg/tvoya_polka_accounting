create table TPA_SHELF (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    SHOP_ID uuid,
    NUMBER_ integer not null,
    STATE integer not null default 10,
    ACTIVE_CONTRACT_ID uuid,
    --
    primary key (ID)
);

alter table TPA_SHELF add constraint FK_TPA_SHELF_ON_SHOP foreign key (SHOP_ID) references TPA_SHOP;
alter table TPA_SHELF add constraint FK_TPA_SHELF_ON_CONTRACT foreign key (ACTIVE_CONTRACT_ID) references TPA_CONTRACT;
create unique index IDX_TPA_SHELF_ON_SHOP_NUMBER_UNQ on TPA_SHELF (SHOP_ID, NUMBER_) where DELETE_TS is null ;
create index IDX_TPA_SHELF_ON_SHOP on TPA_SHELF (SHOP_ID);
create index IDX_TPA_SHELF_ON_ACTIVE_CONTRACT on TPA_SHELF (ACTIVE_CONTRACT_ID);
