alter table TPA_CONTRAGENT rename column address_id to address_id__u35680 ;
drop index IDX_TPA_CONTRAGENT_ON_ADDRESS ;
alter table TPA_CONTRAGENT add column ADDRESS_POSTCODE varchar(16) ^
update TPA_CONTRAGENT set ADDRESS_POSTCODE = '' where ADDRESS_POSTCODE is null ;
alter table TPA_CONTRAGENT alter column ADDRESS_POSTCODE set not null ;
alter table TPA_CONTRAGENT add column ADDRESS_TOWN varchar(64) ^
update TPA_CONTRAGENT set ADDRESS_TOWN = '' where ADDRESS_TOWN is null ;
alter table TPA_CONTRAGENT alter column ADDRESS_TOWN set not null ;
alter table TPA_CONTRAGENT add column ADDRESS_STREET varchar(64) ^
update TPA_CONTRAGENT set ADDRESS_STREET = '' where ADDRESS_STREET is null ;
alter table TPA_CONTRAGENT alter column ADDRESS_STREET set not null ;
alter table TPA_CONTRAGENT add column ADDRESS_BUILDING varchar(8) ^
update TPA_CONTRAGENT set ADDRESS_BUILDING = '' where ADDRESS_BUILDING is null ;
alter table TPA_CONTRAGENT alter column ADDRESS_BUILDING set not null ;
alter table TPA_CONTRAGENT add column ADDRESS_SUBBUILDING varchar(8) ;
alter table TPA_CONTRAGENT add column ADDRESS_FLAT varchar(8) ;
