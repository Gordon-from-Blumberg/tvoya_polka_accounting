alter table TPA_DISCOUNT
    add column SHOP_ID uuid,
    add column CONTRACT_ID uuid,
    add column VALUE integer not null,
    add column END_DATE date;

alter table TPA_DISCOUNT add constraint FK_TPA_DISCOUNT_ON_SHOP foreign key (SHOP_ID) references TPA_SHOP(ID);
alter table TPA_DISCOUNT add constraint FK_TPA_DISCOUNT_ON_CONTRACT foreign key (CONTRACT_ID) references TPA_CONTRACT(ID);

create index IDX_TPA_DISCOUNT_ON_CONTRACT on TPA_DISCOUNT (CONTRACT_ID);
