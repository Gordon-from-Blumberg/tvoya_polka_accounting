alter table TPA_CONTRAGENT alter column SHOP_ID drop not null ;
alter table TPA_INDIVIDUAL_CONTRAGENT add column PASSPORT_ID uuid ;
alter table TPA_DIRECTOR alter column TP_USER_ID drop not null ;
alter table TPA_DIRECTOR alter column GROUP_ID drop not null ;
alter table TPA_RENT_PLACE_TYPE alter column SHOP_ID drop not null ;
alter table TPA_SHOP alter column GROUP_ID drop not null ;
