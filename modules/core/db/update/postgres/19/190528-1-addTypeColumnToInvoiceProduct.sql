delete from TPA_INVOICE_PRODUCT ip where exists
  (select 1 from TPA_INVOICE i where i.TYPE = 30 and ip.INVOICE_ID = i.ID);
delete from TPA_INVOICE where TYPE = 30;

alter table TPA_INVOICE_PRODUCT add column TYPE integer;

update TPA_INVOICE_PRODUCT ip
  set TYPE = i.TYPE
  from TPA_INVOICE i
  where ip.INVOICE_ID = i.ID;

alter table TPA_INVOICE_PRODUCT alter column TYPE set not null;
