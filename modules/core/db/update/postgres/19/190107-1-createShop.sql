create table TPA_SHOP (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    ADDRESS_ID uuid not null,
    WORK_PHONE varchar(20) not null,
    DIRECTOR_ID uuid not null,
    --
    primary key (ID)
);
