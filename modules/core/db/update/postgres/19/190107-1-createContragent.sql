create table TPA_CONTRAGENT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    SHOP_ID uuid not null,
    ADDRESS_ID uuid,
    PHONE varchar(20),
    DEAL_TYPE integer,
    TYPE_ integer not null,
    INN varchar(20),
    OGRN varchar(20),
    KPP varchar(20),
    --
    primary key (ID)
);
