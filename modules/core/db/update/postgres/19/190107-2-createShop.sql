alter table TPA_SHOP add constraint FK_TPA_SHOP_ON_ADDRESS foreign key (ADDRESS_ID) references TPA_ADDRESS(ID);
alter table TPA_SHOP add constraint FK_TPA_SHOP_ON_DIRECTOR foreign key (DIRECTOR_ID) references TPA_TP_USER(ID);
create unique index IDX_TPA_SHOP_UK_ADDRESS_ID on TPA_SHOP (ADDRESS_ID) where DELETE_TS is null ;
create index IDX_TPA_SHOP_ON_ADDRESS on TPA_SHOP (ADDRESS_ID);
create index IDX_TPA_SHOP_ON_DIRECTOR on TPA_SHOP (DIRECTOR_ID);
