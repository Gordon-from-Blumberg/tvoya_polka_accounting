alter table TPA_SHOP alter column ADDRESS_POSTCODE drop not null ;
alter table TPA_SHOP alter column ADDRESS_TOWN drop not null ;
alter table TPA_SHOP alter column ADDRESS_STREET drop not null ;
alter table TPA_SHOP alter column ADDRESS_BUILDING drop not null ;
