alter table TPA_PRODUCT add column ARTICUL bigint;

update TPA_PRODUCT p
set ARTICUL = sub.IDX + sub.base
from (select sub_p.ID, sh.ARTICUL_BASE as BASE, row_number() over (order by sub_p.CREATE_TS) as IDX
      from TPA_PRODUCT sub_p
      inner join TPA_SHOP sh on sh.ID = sub_p.SHOP_ID) sub
where p.ID = sub.ID;

alter table TPA_PRODUCT alter column ARTICUL set not null ;

create unique index IDX_TPA_PRODUCT_ON_ARTICUL_UNQ on TPA_PRODUCT (ARTICUL) where DELETE_TS is null ;
