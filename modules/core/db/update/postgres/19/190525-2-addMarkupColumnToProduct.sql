alter table TPA_PRODUCT
    add column MARKUP decimal(19, 2),
    add column BASE_COST decimal(19, 2);