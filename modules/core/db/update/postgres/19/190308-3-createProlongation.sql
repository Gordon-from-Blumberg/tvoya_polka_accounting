create table TPA_PROLONGATION (
  ID uuid,
  VERSION integer not null,
  CREATE_TS timestamp,
  CREATED_BY varchar(50),
  UPDATE_TS timestamp,
  UPDATED_BY varchar(50),
  DELETE_TS timestamp,
  DELETED_BY varchar(50),
  --
  CONTRACT_ID uuid,
  SHELF_ID uuid,
  RENT_PLACE_TYPE_ID uuid,
  DURATION integer,
  DATE_ date not null,
  START_DATE date,
  END_DATE date,
  COST decimal(19, 2),
  PAID decimal(19, 2),
  NUMBER_ integer not null,
  --
  primary key (ID)
);

alter table TPA_PROLONGATION add constraint FK_TPA_PROLONGATION_ON_CONTRACT foreign key (CONTRACT_ID) references TPA_CONTRACT;
alter table TPA_PROLONGATION add constraint FK_TPA_PROLONGATION_ON_SHELF foreign key (SHELF_ID) references TPA_SHELF;
alter table TPA_PROLONGATION add constraint FK_TPA_PROLONGATION_ON_RENT_PLACE_TYPE foreign key (RENT_PLACE_TYPE_ID) references TPA_RENT_PLACE_TYPE;
create unique index IDX_TPA_PROLONGATION_ON_CONTRACT_NUMBER_UNQ on TPA_PROLONGATION (CONTRACT_ID, NUMBER_) where DELETE_TS is null ;
create index IDX_TPA_PROLONGATION_ON_CONTRACT on TPA_PROLONGATION (CONTRACT_ID);
create index IDX_TPA_PROLONGATION_ON_SHELF on TPA_PROLONGATION (SHELF_ID);
create index IDX_TPA_PROLONGATION_ON_RENT_PLACE_TYPE on TPA_PROLONGATION (RENT_PLACE_TYPE_ID);
