create table TPA_RENT_PLACE_TYPE_TARIFF_LINK (
    RENT_PLACE_TYPE_ID uuid,
    TARIFF_ID uuid,
    primary key (RENT_PLACE_TYPE_ID, TARIFF_ID)
);
