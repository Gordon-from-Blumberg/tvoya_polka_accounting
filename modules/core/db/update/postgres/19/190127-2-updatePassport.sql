alter table TPA_PASSPORT alter column REGISTRY_ADDRESS_POSTCODE drop not null ;
alter table TPA_PASSPORT alter column REGISTRY_ADDRESS_TOWN drop not null ;
alter table TPA_PASSPORT alter column REGISTRY_ADDRESS_STREET drop not null ;
alter table TPA_PASSPORT alter column REGISTRY_ADDRESS_BUILDING drop not null ;
