create table TPA_CONTRACT (
  ID uuid,
  VERSION integer not null,
  CREATE_TS timestamp,
  CREATED_BY varchar(50),
  UPDATE_TS timestamp,
  UPDATED_BY varchar(50),
  DELETE_TS timestamp,
  DELETED_BY varchar(50),
  --
  SHOP_ID uuid,
  CONTRAGENT_ID uuid,
  NUMBER_ integer not null,
  DATE_ date not null,
  END_DATE date,
  CLOSED boolean,
  --
  primary key (ID)
);

alter table TPA_CONTRACT add constraint FK_TPA_CONTRACT_ON_SHOP foreign key (SHOP_ID) references TPA_SHOP;
alter table TPA_CONTRACT add constraint FK_TPA_CONTRACT_ON_CONTRAGENT foreign key (CONTRAGENT_ID) references TPA_CONTRAGENT;
create unique index IDX_TPA_CONTRACT_ON_SHOP_NUMBER_UNQ on TPA_CONTRACT (SHOP_ID, NUMBER_) where DELETE_TS is null ;
create index IDX_TPA_CONTRACT_ON_SHOP on TPA_CONTRACT (SHOP_ID);
create index IDX_TPA_CONTRACT_ON_CONTRAGENT on TPA_CONTRACT (CONTRAGENT_ID);
