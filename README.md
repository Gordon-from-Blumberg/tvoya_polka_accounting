# tvoya_polka_accounting

<b>Accounting system for Tvoya Polka shops</b>

## TODO list
- <b>[DONE]</b> - change logo images and favicon
- <b>[WONT FIXED]</b> - remove user listener for name property
- menu
- <b>[DONE]</b> script for group creating after creating a shop
- <b>[DONE]</b> log
- <b>[DONE]</b> create edit screen for Director
- <b>[DONE]</b> remove all 'NotNull' annotations, use 'required' on web client instead
- <b>[DONE]</b> check cascade deletion
- <b>[DONE]</b> write user friendly entities in logs
- create bean for composing of full name by config
- <b>[IN PROGRESS]</b> add Document entity
- add edit screen for Document
- create field for full name

#### Gradle tasks
- <b>updateDb</b> - updates local db
- <b>deploy start</b> - deploys app and starts local tomcat
