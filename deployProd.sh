#!/bin/sh

buildDir="build"
warDir="${buildDir}/distributions/war"
targetDir="${buildDir}/tmp/deploy"
targetConfDir="${targetDir}/conf/ROOT"
_deployDir="${buildDir}/tmp/_deploy"

echo "clean build dir"
rm -r ${buildDir}/*

mkdir -p ${targetDir}/webapps
mkdir -p ${targetConfDir}
mkdir -p ${targetDir}/conf/Catalina/localhost
mkdir -p ${_deployDir}

coreConfDir="modules/core/conf"
webConfDir="modules/web/conf"

echo "run gradle buildWar"
gradle buildWar

echo "copy war"
cp ${warDir}/app.war ${targetDir}/webapps/ROOT.war

echo "copy conf"
cp ${coreConfDir}/prod.core.app.properties ${targetConfDir}/core.app.properties
cp ${webConfDir}/prod.web.app.properties ${targetConfDir}/web.app.properties
cp conf/ROOT.xml ${targetDir}/conf/Catalina/localhost
cp conf/logback.xml ${targetConfDir}


echo "copy deploy.sh"
cp deploy.sh ${_deployDir}

echo "make archive"
cd ${targetDir}
zip -r ../_deploy/deploy.zip *


echo "upload archive to the server"
scp ../_deploy/* tomcat@80.87.195.237:/opt/tomcat/_deploy
echo "execute deploy.sh on the server"
ssh tomcat@80.87.195.237 sh /opt/tomcat/_deploy/deploy.sh