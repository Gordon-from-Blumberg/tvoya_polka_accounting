#!/usr/bin/env bash

action=$1

if [ -z $action ]
then
 action="start"
fi

if [ $action = "start" ]
then
 echo "start..."
 gradle start

elif [ $action = "deploy" ]
then
 echo "deploy..."
 gradle deploy

elif [ $action = "redeploy" ]
then
 echo "stop, deploy and start again..."
 gradle stop && gradle deploy && gradle start

elif [ $action = "stop" ]
then
 echo "stop..."
 gradle stop

elif [ $action = "update" ]
then
 echo "stop, updateDb and start again..."
 gradle stop && gradle updateDb && gradle start

elif [ $action = "full" ]
then
 echo "full: stop, deploy and update DB, then start again..."
 gradle stop && gradle deploy updateDb && gradle start

elif [ $action = "restart" ]
then
 echo "restart: stop and start again..."
 gradle stop && gradle start
fi
